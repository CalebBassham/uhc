package com.oblivionuhc.uhc.game

import com.oblivionuhc.uhc.UHC
import com.oblivionuhc.uhc.exception.GameConfigurationException
import com.oblivionuhc.uhc.exception.GameNotRunningException
import com.oblivionuhc.uhc.exception.GameRunningException
import com.oblivionuhc.uhc.exception.GameSettingNotRegistered
import com.oblivionuhc.uhc.game.human.GameHost
import com.oblivionuhc.uhc.game.human.GamePlayer
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterGameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterWorldGameSetting
import com.oblivionuhc.uhc.game.setting.scoreboard.TabHealthGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamspeakTimeGameSetting
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamType
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.FinalMapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.MapCenterGameSetting
import com.oblivionuhc.uhc.game.setting.world.MapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.BorderShrinkBlocksGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.BorderShrinkRateGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorderGameSetting
import com.oblivionuhc.uhc.game.spectating.SpectatorManager
import com.oblivionuhc.uhc.game.task.setup.*
import com.oblivionuhc.uhc.game.timer.GameTimer
import com.oblivionuhc.uhc.getGamePlayer
import com.oblivionuhc.uhc.reset
import com.oblivionuhc.uhc.teams.TeamManager
import me.calebbassham.scenariomanager.api.scenarioManager
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scoreboard.Team
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashSet

object Game : Listener {

    var state = GameState.NOT_RUNNING
    var host: GameHost? = null
    var players = ArrayList<GamePlayer>()
    var settings: Set<GameSetting<*>> = emptySet()
        private set

    fun registerSetting(setting: GameSetting<*>, plugin: JavaPlugin) {
        if (setting is Listener) {
            Bukkit.getPluginManager().registerEvents(setting, plugin)
        }

        settings = settings.plus(setting)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : GameSetting<U>, U> getSetting(clazz: Class<T>): T? = settings.firstOrNull { it::class.java == clazz } as T?

    fun <T : GameSetting<U>, U> mustGetSetting(clazz: Class<T>): T = getSetting(clazz)
        ?: throw GameSettingNotRegistered(clazz)

    fun getSetting(name: String) = Optional.ofNullable(settings.firstOrNull { it.name.equals(name, ignoreCase = true) })

    fun start(plugin: JavaPlugin) {
        // Check game is not already running

        if (state == GameState.RUNNING) {
            throw GameRunningException()
        }

        // check for full configuration
        val worlds = mustGetSetting(WorldsGameSetting::class.java).value

        if (worlds.isEmpty()) {
            throw GameConfigurationException("There must be at least one world in the config.")
        }

        val scatterWorld = mustGetSetting(ScatterWorldGameSetting::class.java).value
            .orElseThrow { throw GameConfigurationException("The scatter world must be set in the config.") }

        val mapCenter = mustGetSetting(MapCenterGameSetting::class.java).value
        val mapRadius = mustGetSetting(MapRadiusGameSetting::class.java).value
        val borderShrinkType = mustGetSetting(BorderShrinkTypeGameSetting::class.java).value
        val outsideBorder = mustGetSetting(OutsideBorderGameSetting::class.java).value

        val scatter = mustGetSetting(ScatterGameSetting::class.java).value

        val teamType = mustGetSetting(TeamTypeGameSetting::class.java).value
        val teamSize = mustGetSetting(TeamSizeGameSetting::class.java).value

        val teamspeakTime = mustGetSetting(TeamspeakTimeGameSetting::class.java).value

        // Update state to starting

        state = GameState.STARTING

        // list of setup tasks

        val setupTasks = ArrayList<GameSetupTask>()

        setupTasks.apply {
            add(PlayersGameSetupTask())
            add(ScenarioManagerTeamAssignerScenarioSetupTask())
            if (teamType == TeamType.RANDOM) {
                add(RandomTeamsGameSetupTask(teamSize))
            }
            if (teamspeakTime > 0) {
                add(TeamspeakTimeSetupTask(teamspeakTime, plugin))
            }
            add(WorldsGameSetupTask(worlds, outsideBorder, mapCenter, mapRadius))
            add(FreezeGameSetupTask())
            add(ScoreboardsGameSetupTask())
            add(ResetGamePlayersGameSetupTask())
            add(ScatterGameSetupTask(scatter, scatterWorld, mapCenter, mapRadius))
            add(KillMonstersGameSetupTask())
            add(TimerGameSetupTask(borderShrinkType))
            add(StartingCountdownGameSetupTask(plugin))
            add(UnfreezeGameSetupTask())

            add(object : GameSetupTask() {
                override fun run() {
                    GameTimer.start(plugin)
                    GameTimer.startTime = LocalDateTime.now()

                    if (UHC.instance.isUsingScenarioManager) {
                        scenarioManager.onGameStart(players.mapNotNull { it.player }.toTypedArray())
                    }

                    Game.state = GameState.RUNNING
                    complete()
                }
            })
        }

        for (i in setupTasks.size - 1 downTo 0) {
            if (i != 0) {
                setupTasks[i - 1].completionHandler = Runnable { setupTasks[i].run() }
            } else {
                setupTasks[0].run()
            }
        }

    }

    fun stop() {

        if (state != GameState.RUNNING) {
            throw GameNotRunningException()
        }

        state = GameState.ENDING

        Game.getSetting(TabHealthGameSetting::class.java)?.unregisterObjective()

        GameTimer.stop()
        settings.forEach { it.reset() }

        val worlds = getSetting(WorldsGameSetting::class.java)?.value

        val sendToLobby: (Player) -> Unit = {
            it.reset()

            if (worlds?.contains(it.world) == true) {
                it.teleportAsync(Bukkit.getWorlds().first().spawnLocation)
            }
        }

        players
            .mapNotNull { it.player }
            .forEach(sendToLobby)

        SpectatorManager.spectators.values
            .mapNotNull { it.player }
            .forEach(sendToLobby)

        players.clear()
        host = null
        GameTimer.reset()
        TeamManager.reset()
        SpectatorManager.reset()

        state = GameState.NOT_RUNNING

        if (UHC.instance.isUsingScenarioManager) {
            scenarioManager.onGameStop()
        }
    }

    fun getExponentialShrink(shrink: Int): Int {
        val radius = mustGetSetting(MapRadiusGameSetting::class.java).value
        val finalRadius = mustGetSetting(FinalMapRadiusGameSetting::class.java).value
        val decayRate = 1 - mustGetSetting(BorderShrinkRateGameSetting::class.java).value

        var newRadius = Math.floor((radius - finalRadius) * Math.pow(decayRate, shrink.toDouble()) + 50)

        newRadius -= newRadius % 2

        return if (newRadius < finalRadius) finalRadius else newRadius.toInt()
    }

    fun getContinuousShrink(shrink: Int): Double {
        val radius = mustGetSetting(MapRadiusGameSetting::class.java).value
        val finalRadius = mustGetSetting(FinalMapRadiusGameSetting::class.java).value
        val blocks = mustGetSetting(BorderShrinkBlocksGameSetting::class.java).value

        val newRadius = radius - (shrink * blocks)

        return if (newRadius < finalRadius) finalRadius.toDouble() else newRadius
    }

    fun getSolos(): List<GamePlayer> = players.filter { it.alive && Bukkit.getScoreboardManager().mainScoreboard.getPlayerTeam(it.offlinePlayer) == null }

    fun getTeams(): Set<Team> {
        val teams = HashSet<Team>()

        players.filter { it.alive }.forEach { gamePlayer ->
            TeamManager.scoreboard.getEntryTeam(gamePlayer.name)?.let { team ->
                teams.add(team)
            }
        }

        return teams
    }

    fun isTeamGame() = getTeams().isNotEmpty()

    @EventHandler
    fun onGamePlayerDeath(e: PlayerDeathEvent) {
        val gamePlayer = e.entity.getGamePlayer() ?: return
        gamePlayer.alive = false
    }

}