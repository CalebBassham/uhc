package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.XZLocation
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.scatter.Scatter
import org.bukkit.World

class ScatterGameSetupTask(private val scatter: Scatter, private val scatterWorld: World, private val mapCenter: XZLocation, private val mapRadius: Int) : GameSetupTask() {

    override fun run() {
        scatter.mixed(Game.getSolos().mapNotNull { it.player }, Game.getTeams(), scatterWorld, mapCenter, mapRadius).whenComplete { _, _ ->
            complete()
        }
    }

}