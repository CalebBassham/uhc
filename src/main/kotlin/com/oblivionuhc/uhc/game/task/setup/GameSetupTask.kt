package com.oblivionuhc.uhc.game.task.setup

abstract class GameSetupTask : Runnable {

    var completionHandler: Runnable? = null

    fun complete() {
        completionHandler?.run()
    }

}
