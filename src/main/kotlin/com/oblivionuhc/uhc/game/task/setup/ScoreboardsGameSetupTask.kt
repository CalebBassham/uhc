package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.scoreboard.TabHealthGameSetting

class ScoreboardsGameSetupTask : GameSetupTask() {

    override fun run() {
        val tabHealth = Game.getSetting(TabHealthGameSetting::class.java)

        when (tabHealth?.value) {
            TabHealthGameSetting.Type.HEARTS,
            TabHealthGameSetting.Type.INTEGER -> tabHealth.registerObjective()
            TabHealthGameSetting.Type.NONE -> tabHealth.unregisterObjective()
        }

        complete()
    }

}
