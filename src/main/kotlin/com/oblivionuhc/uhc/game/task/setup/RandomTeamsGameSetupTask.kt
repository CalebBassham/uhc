package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.exception.GameSetupException
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.lang.broadcast
import com.oblivionuhc.uhc.teams.TeamManager
import java.util.*

class RandomTeamsGameSetupTask(private val teamSize: Int) : GameSetupTask() {

    override fun run() {
        if (Game.players.size / teamSize < 2) { // TODO: Math?
            throw GameSetupException("There are not enough players to create rTo$teamSize.")
        }

        val totalTeams = Math.ceil(Game.players.size / teamSize.toDouble()).toInt()

        val players = LinkedList(Game.players.map { it.offlinePlayer })

        for (i in 0 until totalTeams) {
            val team = TeamManager.create()
            for (j in 0 until teamSize) {
                if (players.isEmpty()) {
                    break
                }

                team.addPlayer(players.poll())
            }
        }

        broadcast("created_random_team", mapOf(Pair("team_size", teamSize.toString())))

        complete()
    }
}