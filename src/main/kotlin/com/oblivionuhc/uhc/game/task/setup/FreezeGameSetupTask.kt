package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.freeze.freezer
import com.oblivionuhc.uhc.game.Game

class FreezeGameSetupTask : GameSetupTask() {

    override fun run() {
        Game.players.map { it.offlinePlayer }.forEach { freezer.freeze(it) }
        complete()
    }

}