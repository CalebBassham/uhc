package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.scatter.ScatterWorldGameSetting
import org.bukkit.entity.Monster

class KillMonstersGameSetupTask : GameSetupTask() {
    override fun run() {
        val world = Game.getSetting(ScatterWorldGameSetting::class.java)?.value?.orElse(null) ?: return
        world.entities
            .filter { it is Monster }
            .map { it as Monster }
            .forEach { it.isInDaylight || it.location.y > 60 }
        complete()
    }
}