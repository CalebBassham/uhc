package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.reset
import org.bukkit.GameMode

class ResetGamePlayersGameSetupTask : GameSetupTask() {

    override fun run() {
        Game.players.mapNotNull { it.player }.forEach {
            it.reset()
            it.gameMode = GameMode.SURVIVAL
        }

        complete()
    }

}