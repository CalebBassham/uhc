package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.human.GamePlayer
import com.oblivionuhc.uhc.game.spectating.SpectatorManager.getSpectator
import org.bukkit.Bukkit

class PlayersGameSetupTask : GameSetupTask() {

    override fun run() {
        val players = Bukkit.getOnlinePlayers()
            .filter { it.getSpectator() == null }

        // TODO("Stop game if not enough players")

        Game.players = ArrayList(players.map { GamePlayer(it) })
        complete()
    }

}