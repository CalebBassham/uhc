package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.time.EndgameTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.FinalHealTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.PvPTimeGameSetting
import com.oblivionuhc.uhc.game.setting.world.FinalMapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.BorderShrinkIntervalGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkType
import com.oblivionuhc.uhc.game.timer.GameTimer
import com.oblivionuhc.uhc.game.timer.event.BorderShrinkGameTimerEvent
import com.oblivionuhc.uhc.game.timer.event.FinalHealGameTimerEvent
import com.oblivionuhc.uhc.game.timer.event.PvPGameTimerEvent

class TimerGameSetupTask(private val borderShrinkType: BorderShrinkType) : GameSetupTask() {

    override fun run() {
        GameTimer.registerEvent(FinalHealGameTimerEvent(Game.mustGetSetting(FinalHealTimeGameSetting::class.java).value))
        GameTimer.registerEvent(PvPGameTimerEvent(Game.mustGetSetting(PvPTimeGameSetting::class.java).value))

        when (borderShrinkType) {
            BorderShrinkType.EXPONENTIAL -> registerExponentialBorderShrinks()
            BorderShrinkType.CONTINUOUS -> registerContinuousBorderShrinks()
        }

        complete()
    }

    private data class BorderShrink(val radius: Double, var ticks: Long)

    private fun registerExponentialBorderShrinks() {
        val finalRadius = Game.mustGetSetting(FinalMapRadiusGameSetting::class.java).value
        var ticks = Game.mustGetSetting(EndgameTimeGameSetting::class.java).value
        val shrinkInterval = Game.mustGetSetting(BorderShrinkIntervalGameSetting::class.java).value

        var i = 1

        val shrinks = ArrayList<BorderShrink>()

        while (true) {
            val radius = Game.getExponentialShrink(i)

            shrinks.add(BorderShrink(radius.toDouble(), ticks))

            if (radius <= finalRadius) {
                break
            }

            ticks += shrinkInterval
            i++
        }

        combineSameRadiusShrinks(shrinks)
            .forEach { GameTimer.registerEvent(BorderShrinkGameTimerEvent(it.radius, it.ticks)) }
    }

    private fun combineSameRadiusShrinks(shrinks: List<BorderShrink>): List<BorderShrink> {

        for (i in 0 until shrinks.size - 1) {
            val shrink = shrinks[i]
            val next = shrinks[i + 1]

            if (shrink.radius == next.radius) {
                return combineSameRadiusShrinks(shrinks.minus(shrink))
            }

        }

        return shrinks
    }

    private fun registerContinuousBorderShrinks() {
        val finalRadius = Game.mustGetSetting(FinalMapRadiusGameSetting::class.java).value
        var ticks = Game.mustGetSetting(EndgameTimeGameSetting::class.java).value
        val shrinkInterval = Game.mustGetSetting(BorderShrinkIntervalGameSetting::class.java).value

        var i = 1

        while (true) {
            val radius = Game.getContinuousShrink(i)

            GameTimer.registerEvent(BorderShrinkGameTimerEvent(radius, ticks))

            if (radius <= finalRadius) {
                break
            }

            ticks += shrinkInterval
            i++
        }
    }
}