package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.orUndefinedMessage
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable

class StartingCountdownGameSetupTask(private val plugin: JavaPlugin) : GameSetupTask() {

    override fun run() {
        object : BukkitRunnable() {
            var seconds = 30
            override fun run() {
                if (seconds <= 0) {
                    cancel()
                    complete()
                    return
                }

                Bukkit.getOnlinePlayers().forEach { player ->
                    val title = LanguageManager.getMessage("game_starting_title", player)
                        ?.replaceVariable("seconds", seconds.toString())
                        .orUndefinedMessage(player)
                        .message

                    val subtitle = LanguageManager.getMessage("game_starting_subtitle", player)
                        .orUndefinedMessage(player)
                        .message

                    player.sendTitle(title, subtitle, 0, 25, 0)
                }

                seconds--
            }
        }.runTaskTimer(plugin, 0, 20)
    }

}