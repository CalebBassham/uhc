package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.XZLocation
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.world.border.BorderDamageBufferGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.BorderDamageGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorder
import org.bukkit.World
import org.bukkit.entity.Monster

class WorldsGameSetupTask(private val worlds: Set<World>, private val outsideBorder: OutsideBorder, private val mapCenter: XZLocation, private val mapRadius: Int) : GameSetupTask() {

    override fun run() {
        worlds.forEach {
            it.pvp = false
            it.time = 50
            it.entities.filterIsInstance(Monster::class.java).forEach { it.remove() }
            it.setStorm(false)

            // border
            val border = it.worldBorder
            border.setCenter(mapCenter.x, mapCenter.z)
            border.size = mapRadius * 2.0

            when (outsideBorder) {
                OutsideBorder.DAMAGE -> border.damageAmount = Game.mustGetSetting(BorderDamageGameSetting::class.java).value
                OutsideBorder.KILL -> border.damageAmount = 20.0
                OutsideBorder.TELEPORT -> border.damageAmount = 0.00001
            }

            when (outsideBorder) {
                OutsideBorder.DAMAGE,
                OutsideBorder.KILL -> border.damageBuffer = Game.mustGetSetting(BorderDamageBufferGameSetting::class.java).value
                OutsideBorder.TELEPORT -> border.damageBuffer = 0.0
            }

            border.warningDistance = 0
            border.warningTime = 0
        }

        complete()
    }

}
