package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.game.Game
import me.calebbassham.scenariomanager.api.scenarioManager

class ScenarioManagerTeamAssignerScenarioSetupTask : GameSetupTask() {

    override fun run() {
        val players = Game.players.mapNotNull { it.player }.toTypedArray()

        scenarioManager.onAssignTeams(players).whenComplete { _, _ -> complete() }
    }
}