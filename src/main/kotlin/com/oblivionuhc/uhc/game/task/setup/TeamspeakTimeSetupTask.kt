package com.oblivionuhc.uhc.game.task.setup

import com.oblivionuhc.uhc.formatTicks
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.orUndefinedMessage
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable

class TeamspeakTimeSetupTask(private val teamspeakTimeTicks: Long, private val plugin: JavaPlugin) : GameSetupTask() {

    override fun run() {
        var ticks = teamspeakTimeTicks

        object : BukkitRunnable() {
            override fun run() {
                Bukkit.getOnlinePlayers().forEach { player ->
                    val title = LanguageManager.getMessage("join_teamspeak_title", player)
                        ?.replaceVariable("time_remaining", formatTicks(ticks))
                        .orUndefinedMessage(player)
                        .message

                    val subtitle = LanguageManager.getMessage("join_teamspeak_subtitle", player)
                        .orUndefinedMessage(player)
                        .message

                    player.sendTitle(title, subtitle, 0, 25, 0)
                }

                ticks -= 20

                if (ticks <= 0) {
                    cancel()
                    complete()
                }
            }
        }.runTaskTimer(plugin, 0, 20)

    }

}