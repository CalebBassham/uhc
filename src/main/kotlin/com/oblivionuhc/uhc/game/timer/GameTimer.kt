package com.oblivionuhc.uhc.game.timer

import com.oblivionuhc.uhc.exception.GameTimerNotRunningException
import com.oblivionuhc.uhc.exception.GameTimerRunningException
import com.oblivionuhc.uhc.formatTicks
import com.oblivionuhc.uhc.game.spectating.SpectatorManager
import com.oblivionuhc.uhc.game.timer.event.GameTimerEvent
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.orUndefinedMessage
import com.oblivionuhc.uhc.scenarioManager.ScenarioEventGameTimerEvent
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scheduler.BukkitTask
import java.time.LocalDateTime
import java.util.*

object GameTimer {

    var ticks: Long = 0
        private set
    var events: Set<GameTimerEvent> = emptySet()
        private set
    var completedEvents: Set<GameTimerEvent> = emptySet()
        private set
    private var task: BukkitTask? = null
    var startTime: LocalDateTime? = null

    fun registerEvent(event: GameTimerEvent) {
        events = events.plus(event)
    }

    fun start(plugin: JavaPlugin) {
        if (this.task != null) {
            throw GameTimerRunningException()
        }

        if (startTime == null) {
            startTime = LocalDateTime.now()
        }

        val task = object : BukkitRunnable() {
            override fun run() {
                if (ticks % 20 == 0.toLong()) {
                    events
                        .filterNot { it.hide }
                        .sortedBy { it.ticksRemaining }
                        .firstOrNull()
                        ?.let { event ->
                            Bukkit.getOnlinePlayers().forEach {
                                val msg = LanguageManager.getMessage("game_timer", it)
                                    ?.replaceVariable("name", event.name)
                                    ?.replaceVariable("time_remaining", formatTicks(event.ticksRemaining))
                                    .orUndefinedMessage(it)
                                    .message

                                it.sendActionBar(msg)
                            }
                        }
                }

                events.filterIsInstance(ScenarioEventGameTimerEvent::class.java).forEach { it.onTick(it.ticksRemaining) }

                if (ticks % 3600 == 0.toLong()) {
                    SpectatorManager.minedLocations.clear() // Clear up some memory
                }

                ticks++

                val ready = events.filter { it.ticksRemaining <= 0 }
                ready.forEach { it.run() }
                events = events.minus(ready)
                completedEvents = completedEvents.plus(ready)
            }
        }

        this.task = task.runTaskTimer(plugin, 0, 1)
    }

    fun stop() {
        val task = this.task
        task ?: throw GameTimerNotRunningException()

        task.cancel()

        this.task = null
    }

    fun reset() {
        this.ticks = 0
        this.events = emptySet()
        this.completedEvents = emptySet()
        try {
            stop()
        } catch (e: GameTimerNotRunningException) {
        }
        this.startTime = null
    }

    fun nextEvent(): Optional<GameTimerEvent> = Optional.ofNullable(events.sortedBy { it.ticksRemaining }.firstOrNull())

}



