package com.oblivionuhc.uhc.game.timer.event

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.lang.broadcast
import org.bukkit.attribute.Attribute

class FinalHealGameTimerEvent(ticks: Long) : GameTimerEvent("Final heal", ticks) {

    override fun run() {
        Game.players
            .mapNotNull { it.player }
            .forEach { it.health = it.getAttribute(Attribute.GENERIC_MAX_HEALTH).defaultValue }
        broadcast("final_heal")
    }
}