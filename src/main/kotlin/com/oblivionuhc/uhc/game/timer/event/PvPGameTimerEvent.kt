package com.oblivionuhc.uhc.game.timer.event

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import com.oblivionuhc.uhc.lang.broadcast

class PvPGameTimerEvent(ticks: Long) : GameTimerEvent("PvP", ticks) {

    override fun run() {
        Game.getSetting(WorldsGameSetting::class.java)?.let { it.value.forEach { it.pvp = true } }
        broadcast("pvp_enabled")
    }
}