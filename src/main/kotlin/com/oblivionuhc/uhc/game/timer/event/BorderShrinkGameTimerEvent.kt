package com.oblivionuhc.uhc.game.timer.event

import com.oblivionuhc.uhc.event.BorderShrinkEvent
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import com.oblivionuhc.uhc.lang.broadcast
import org.bukkit.Bukkit

class BorderShrinkGameTimerEvent(val newRadius: Double, ticks: Long) : GameTimerEvent("Border Shrink » ±$newRadius blocks", ticks) {

    override fun run() {
        Game.getSetting(WorldsGameSetting::class.java)?.let {
            val borderShrinkEvent = BorderShrinkEvent(newRadius)
            Bukkit.getPluginManager().callEvent(borderShrinkEvent)

            if (!borderShrinkEvent.isCancelled) {
                it.value.forEach {
                    it.worldBorder.size = newRadius * 2.0
                    broadcast("border_shrink", mapOf(Pair("radius", newRadius.toString())))
                }
            }
        }
    }
}