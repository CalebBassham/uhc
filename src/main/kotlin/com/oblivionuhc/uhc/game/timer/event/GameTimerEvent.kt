package com.oblivionuhc.uhc.game.timer.event

import com.oblivionuhc.uhc.game.timer.GameTimer

abstract class GameTimerEvent(val name: String, val ticks: Long, val hide: Boolean = false) : Runnable {

    val ticksRemaining
        get() = ticks - GameTimer.ticks

    open fun onTick(ticksRemaining: Long) {}

}