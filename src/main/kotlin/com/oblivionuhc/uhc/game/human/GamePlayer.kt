package com.oblivionuhc.uhc.game.human

import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player
import java.util.*

class GamePlayer(val name: String, val uuid: UUID) {

    constructor(player: Player) : this(player.displayName, player.uniqueId)

    var scattered = false
    var alive = true

    var goldMined = 0
    var diamondsMined = 0

    val player: Player?
        get() = Bukkit.getPlayer(uuid)

    val offlinePlayer: OfflinePlayer
        get() = Bukkit.getOfflinePlayer(uuid)

}