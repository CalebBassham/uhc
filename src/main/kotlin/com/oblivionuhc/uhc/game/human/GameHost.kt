package com.oblivionuhc.uhc.game.human

import org.bukkit.Bukkit
import org.bukkit.entity.Player
import java.util.*

class GameHost(val name: String, val uuid: UUID) {

    val player: Player?
        get() = Bukkit.getPlayer(uuid)

}