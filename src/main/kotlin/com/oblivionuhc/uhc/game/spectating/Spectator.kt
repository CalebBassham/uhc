package com.oblivionuhc.uhc.game.spectating

import com.oblivionuhc.uhc.exception.PlayerOfflineException
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player
import java.util.*

class Spectator(val uuid: UUID, var type: Spectator.Type = Spectator.Type.FREE) {

    var isSpectating = false
        set(value) {
            if (value == field) return
            if (value) startSpectating() else stopSpectating()
            field = value
        }

    private fun startSpectating() {
        val player = player ?: throw PlayerOfflineException(offlinePlayer)
        var specTarget: Player? = null

        if(type == Type.FREE) {
            LanguageManager.getMessage("spectating_start", player).send(player)

            if (player.hasPermission("uhc.spectate.chat.spectator") && !player.hasPermission("uhc.spectate.chat.general")) {
                LanguageManager.getMessage("spectating_speak_normal", player).send(player)
            } else if (player.hasPermission("uhc.spectate.chat.spectator") && player.hasPermission("uhc.spectate.chat.general")) {
                LanguageManager.getMessage("spectating_speak_command", player).send(player)
            } else {
                LanguageManager.getMessage("spectating_no_speak", player).send(player)
            }
        } else if (type == Type.TEAM_ONLY) {
            specTarget = SpectatorManager.getNextPlayerToSpectate(this)
            if (specTarget == null) {
                LanguageManager.getMessage("no_teammates_to_spectate", player).send(player)
                return
            }
        }

        player.gameMode = GameMode.SPECTATOR

        specTarget?.let {
            player.teleportAsync(specTarget.location).whenComplete { _, _ ->
                player.spectatorTarget = it
            }
        }
    }

    private fun stopSpectating() {
        val player = player ?: throw PlayerOfflineException(offlinePlayer)

        player.gameMode = GameMode.SURVIVAL
        player.teleportAsync(Bukkit.getWorlds()[0].spawnLocation) //TODO: Something better

        LanguageManager.getMessage("spectating_stop", player).send(player)
    }

    val player: Player?
        get() = Bukkit.getPlayer(uuid)

    val offlinePlayer: OfflinePlayer
        get() = Bukkit.getOfflinePlayer(uuid)

    fun sendChatMessage(msg: String) {
        SpectatorManager.spectators.values
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spectator_chat", it)
                    ?.replaceVariable("player", it.displayName)
                    ?.replaceVariable("message", msg)
            }
    }

    enum class Type {
        FREE, TEAM_ONLY
    }

}