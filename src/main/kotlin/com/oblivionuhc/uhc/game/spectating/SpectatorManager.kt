package com.oblivionuhc.uhc.game.spectating

import com.destroystokyo.paper.event.player.PlayerStopSpectatingEntityEvent
import com.oblivionuhc.uhc.UHC
import com.oblivionuhc.uhc.format
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.getGamePlayer
import com.oblivionuhc.uhc.getNearbyBlocks
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import com.oblivionuhc.uhc.operator.plus
import com.oblivionuhc.uhc.teams.TeamManager.getTeam
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.GameMode
import org.bukkit.Material
import org.bukkit.entity.Entity
import org.bukkit.entity.FallingBlock
import org.bukkit.entity.Player
import org.bukkit.entity.Projectile
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityDamageByBlockEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryOpenEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.event.player.*
import org.bukkit.projectiles.BlockProjectileSource
import org.bukkit.util.Vector
import org.spigotmc.event.entity.EntityDismountEvent
import java.text.DecimalFormat
import java.util.*

object SpectatorManager : Listener {

    val spectators = HashMap<UUID, Spectator>()
    private var playerIndex = -1 // hacky but works

    fun reset() {
        spectators.forEach { it.value.isSpectating = false }
        spectators.clear()
    }

    fun getNextPlayerToSpectate(spec: Spectator): Player? =
        when (spec.type) {
            Spectator.Type.FREE -> nextSpectatorTargetForFreeSpectator()
            Spectator.Type.TEAM_ONLY -> nextSpectatorTargetForTeamOnlySpectator(spec)
        }

    private fun nextSpectatorTargetForFreeSpectator(): Player? {
        if (Game.players.isEmpty()) {
            return null
        }

        while (true) {
            playerIndex++
            if (playerIndex > Game.players.size - 1) {
                playerIndex = 0
            }

            return Game.players[playerIndex].player ?: continue
        }
    }

    private fun nextSpectatorTargetForTeamOnlySpectator(spec: Spectator): Player? {
        val team = spec.player?.getTeam() ?: return null

        val aliveMembers = team.entries
            .mapNotNull { Bukkit.getPlayer(it) }
            .mapNotNull { it.getGamePlayer() }
            .filter { it.alive }
            .sortedBy { it.name }

        if (aliveMembers.isEmpty()) return null

        val spectator = spec.player ?: return null

        var currentIndex: Int = 0

        for (i in 0 until aliveMembers.size) {
            val member = aliveMembers[i]

            if (spectator.spectatorTarget == member.player) {
                currentIndex = i
            }
        }

        if (currentIndex != aliveMembers.lastIndex) {
            return aliveMembers[currentIndex + 1].player
        } else {
            return aliveMembers[0].player
        }
    }

    fun Player.getSpectator(): Spectator? = spectators[this.uniqueId]

    @EventHandler
    fun onLeftClick(e: PlayerInteractEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return
        if (e.action != Action.LEFT_CLICK_AIR && e.action != Action.LEFT_CLICK_BLOCK) return
        if (e.player.gameMode != GameMode.SPECTATOR && e.player.hasPermission("uhc.spectate.break")) return
        val target = getNextPlayerToSpectate(spectator)

        if (target == null) {
            LanguageManager.getMessage("spec_nobody_tp", e.player).send(e.player)
            return
        }

        e.player.teleport(target)

        LanguageManager.getMessage("spec_tp", e.player)
            ?.replaceVariable("player", target.displayName)
            .send(e.player)
    }

    @EventHandler
    fun onChat(e: AsyncPlayerChatEvent) {
        val spectator = e.player.getSpectator() ?: return

        if (e.player.hasPermission("uhc.spectate.chat.spectator") && !e.player.hasPermission("uhc.spectate.chat.general")) {
            e.isCancelled = true
            spectator.sendChatMessage(e.message)
        } else if (e.player.hasPermission("uhc.spectate.chat.spectator") && !e.player.hasPermission("uhc.spectate.chat.general")) {
            e.isCancelled = true
            e.player.sendMessage(ChatColor.RED + "You do not have permission to speak for the duration of the game because you are isSpectating.")
        }
    }

    @EventHandler
    fun onDisconnect(e: PlayerQuitEvent) {
        val spectator = e.player.getSpectator() ?: return
        spectator.isSpectating = false
    }

    @EventHandler
    fun onStopSpectating(e: EntityDismountEvent) {
        val player = e.entity as? Player ?: return

        UHC.logger.info("${player.displayName} dismounted ${e.dismounted.name}")
    }

    /*
    * ---------------------------------------------
    * SPEC INFO
    * ---------------------------------------------
    */


    /* Damage */

    private val damageFormat = DecimalFormat("#.#")


    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onPvPDamage(e: EntityDamageByEntityEvent) {
        val attacker = e.damager as? Player ?: return
        val victim = e.entity as? Player ?: return

        attacker.getGamePlayer() ?: return
        victim.getGamePlayer() ?: return

        spectators
            .map { it.value }
            .filter { it.type != Spectator.Type.TEAM_ONLY }
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spec_info_damage_alert", it)
                    ?.replaceVariable("victim", victim.displayName)
                    ?.replaceVariable("attacker", attacker.displayName)
                    ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                    ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                    .send(it)
            }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onPvEEntityDamage(e: EntityDamageByEntityEvent) { // also includes shooter damage, entities and blocks ;)
        val attacker = e.damager                          // also also includes falling blocks
        if (attacker is Projectile) {                     // also also also is a mess
            val shooter = attacker.shooter
            if (shooter is Entity) { // repeated below for block shooters (dispensers)
                val victim = e.entity as? Player ?: return

                victim.getGamePlayer() ?: return

                spectators
                    .map { it.value }
                    .filter { it.type != Spectator.Type.TEAM_ONLY }
                    .mapNotNull { it.player }
                    .forEach {
                        LanguageManager.getMessage("spec_info_damage_alert", it)
                            ?.replaceVariable("victim", victim.displayName)
                            ?.replaceVariable("attacker", shooter.name + "(" + attacker.type.name.toLowerCase().replace("_", " ") + ")")
                            ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                            ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                            .send(it)
                    }
                return
            } else if (shooter is BlockProjectileSource) { // dispenser
                val victim = e.entity as? Player ?: return

                // TODO: show type of spectral arrow

                victim.getGamePlayer() ?: return

                spectators
                    .map { it.value }
                    .filter { it.type != Spectator.Type.TEAM_ONLY }
                    .mapNotNull { it.player }
                    .forEach {
                        LanguageManager.getMessage("spec_info_damage_alert", it)
                            ?.replaceVariable("victim", victim.displayName)
                            ?.replaceVariable("attacker", attacker.name + "(" + attacker.type.name.toLowerCase().replace("_", " ") + ")")
                            ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                            ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                            .send(it)
                    }
                return
            }
        } else if (attacker is Player) {
            return
        }

        val victim = e.entity as? Player ?: return

        victim.getGamePlayer() ?: return

        spectators
            .map { it.value }
            .filter { it.type != Spectator.Type.TEAM_ONLY }
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spec_info_damage_alert", it)
                    ?.replaceVariable("victim", victim.displayName)
                    ?.replaceVariable("attacker", if (attacker is FallingBlock) attacker.material.name.toLowerCase() else attacker.name)
                    ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                    ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                    .send(it)
            }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onPvEBlockDamage(e: EntityDamageByBlockEvent) {
        val attacker = e.damager ?: return
        val victim = e.entity as? Player ?: return

        victim.getGamePlayer() ?: return

        spectators
            .map { it.value }
            .filter { it.type != Spectator.Type.TEAM_ONLY }
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spec_info_damage_alert", it)
                    ?.replaceVariable("victim", victim.displayName)
                    ?.replaceVariable("attacker", attacker.type.name.toLowerCase())
                    ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                    ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                    .send(it)
            }

        val nearbyPlayers = victim.world.players.minus(victim)
            .filter { it.getGamePlayer() != null }
            .filter { it.location.distanceSquared(victim.location) < 625 /* 25 blocks */ }

        if (!nearbyPlayers.isEmpty()) {
            spectators.mapNotNull { it.value.player }.forEach {
                LanguageManager.getMessage("spec_info_nearby_players", it)
                    ?.replaceVariable("players", nearbyPlayers.format())
                    .send(it)
            }
        }
    }

    private val ignoreDamageCauses = arrayOf(EntityDamageEvent.DamageCause.CONTACT, EntityDamageEvent.DamageCause.ENTITY_ATTACK,
        EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK, EntityDamageEvent.DamageCause.PROJECTILE, EntityDamageEvent.DamageCause.FALLING_BLOCK,
        EntityDamageEvent.DamageCause.HOT_FLOOR)

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun entityDamageEvent(e: EntityDamageEvent) {
        if (ignoreDamageCauses.contains(e.cause)) {
            return
        }

        val victim = e.entity as? Player ?: return
        victim.getGamePlayer() ?: return

        spectators
            .map { it.value }
            .filter { it.type != Spectator.Type.TEAM_ONLY }
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spec_info_damage_alert", it)
                    ?.replaceVariable("victim", victim.displayName)
                    ?.replaceVariable("attacker", e.cause.name.toLowerCase().replace("_", " "))
                    ?.replaceVariable("damage_done", damageFormat.format(e.finalDamage))
                    ?.replaceVariable("remaining_health", damageFormat.format(Math.min(victim.health - e.finalDamage, 0.0)))
                    .send(it)
            }
    }

    /* Mining */

    val minedLocations = HashSet<Vector>()

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onMine(e: BlockBreakEvent) {
        val player = e.player
        val gamePlayer = player.getGamePlayer() ?: return
        if (!gamePlayer.alive) {
            return
        }

        val loc = e.block.location
        if (minedLocations.contains(loc.toVector())) {
            return
        }

        if (loc.block.type != Material.GOLD_ORE && loc.block.type != Material.DIAMOND_ORE) {
            return
        }

        val vein = loc.getNearbyBlocks(3)
            .filter { it.type == loc.block.type }

        minedLocations.addAll(vein.map { it.location.toVector() })

        if (loc.block.type == Material.GOLD_ORE) {
            gamePlayer.goldMined += vein.size
        } else {
            gamePlayer.diamondsMined += vein.size
        }

        spectators
            .map { it.value }
            .filter { it.type != Spectator.Type.TEAM_ONLY }
            .mapNotNull { it.player }
            .forEach {
                LanguageManager.getMessage("spec_info_mining_alert", it)
                    ?.replaceVariable("player", player.displayName)
                    ?.replaceVariable("ore", e.block.type.name.toLowerCase().replace("_", " "))
                    ?.replaceVariable("vein", vein.size.toString())
                    ?.replaceVariable("total", (if (loc.block.type == Material.GOLD_ORE) gamePlayer.goldMined else gamePlayer.diamondsMined).toString())
                    .send(it)
            }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onPlace(e: BlockPlaceEvent) {
        val gamePlayer = e.player.getGamePlayer() ?: return
        if (!gamePlayer.alive) {
            return
        }

        if (e.block.type == Material.GOLD_ORE || e.block.type == Material.DIAMOND_ORE) {
            minedLocations.add(e.block.location.toVector())
        }
    }

    /*
    * ------------------------------------
    * Open Inventories
    * ------------------------------------
    */

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onRightClickPlayer(e: PlayerInteractAtEntityEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return

        val target = e.rightClicked as? Player ?: return
        e.player.openInventory(target.inventory)
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onInventoryClick(e: InventoryClickEvent) {
        val clicker = e.whoClicked as? Player ?: return
        val spectator = clicker.getSpectator() ?: return
        if (!spectator.isSpectating) return

        if (e.inventory.holder == clicker) return
        if (e.inventory.type == InventoryType.CREATIVE) return

        e.isCancelled = true
    }

    @EventHandler
    fun onTeamSpectatorInventoryOpen(e: InventoryOpenEvent) {
        val player = e.player as Player
        val spec = player.getSpectator() ?: return

        if(!spec.isSpectating) return

        val specTarget = player.spectatorTarget as? Player ?: return
        if (specTarget == player) return

        e.isCancelled = true
        player.openInventory(specTarget.inventory)
    }

    /*
    * -----------------------------------
    * Guards
    * ---------------------------------- */

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onSpectatorPlace(e: BlockPlaceEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return
        if (e.player.hasPermission("uhc.spectate.place")) return

        e.isCancelled = true
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onSpectatorBreak(e: BlockBreakEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return
        if (e.player.hasPermission("uhc.spectate.break")) return

        e.isCancelled = true
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onSpectatorItemDrop(e: PlayerDropItemEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return
        if (e.player.hasPermission("uhc.spectate.drop")) return

        e.isCancelled = true
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onSpectatorGamemodeChange(e: PlayerGameModeChangeEvent) {
        val spectator = e.player.getSpectator() ?: return
        if (!spectator.isSpectating) return
        if (e.newGameMode == GameMode.SPECTATOR) return
        if (e.player.hasPermission("uhc.spectate.gamemode")) return

        e.isCancelled = true
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onSpectatorDamage(e: EntityDamageEvent) {
        val player = e.entity as? Player ?: return
        val spectator = player.getSpectator() ?: return
        if (!spectator.isSpectating) return

        e.isCancelled = true
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onTeamSpectatorStopSpectating(e: PlayerStopSpectatingEntityEvent) {
        val spec = e.player.getSpectator() ?: return
        if (!spec.isSpectating) return
        if (spec.type != Spectator.Type.TEAM_ONLY) return

        // try to start spectating next alive teammate
        val specTarget = e.spectatorTarget as? Player
        if (specTarget != null) {
            if (specTarget.getGamePlayer()?.alive == false) {
                val nextTarget = getNextPlayerToSpectate(spec)
                if (nextTarget != null) {
                    e.player.spectatorTarget = nextTarget
                    return
                }
            }
        }

        spec.isSpectating = false
    }

}