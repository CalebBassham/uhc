package com.oblivionuhc.uhc.game

enum class GameState {

    NOT_RUNNING, STARTING, RUNNING, ENDING

}