package com.oblivionuhc.uhc.game.setting

interface MaximumValueGameSetting<out T : Number> {
    val maximumValue: T
}
