package com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType

enum class BorderShrinkType {

    EXPONENTIAL, CONTINUOUS

}