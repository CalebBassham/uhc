package com.oblivionuhc.uhc.game.setting

import com.oblivionuhc.uhc.UHC
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting
import com.oblivionuhc.uhc.game.spectating.Spectator
import com.oblivionuhc.uhc.game.spectating.SpectatorManager
import com.oblivionuhc.uhc.getGamePlayer
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.scheduler.BukkitRunnable

class SpectateTeamatesAfterDeathGameSetting : GameSetting<Boolean>("Spectate Teammates After Death", true, true), Listener {

    override var value: Boolean = defaultValue
        get() {
            if (!Game.isTeamGame()) return false

            return field
        }

    override fun display(): Boolean {
        val teamSize = Game.getSetting(TeamSizeGameSetting::class.java)?.value ?: 1
        return teamSize > 1
    }

    @EventHandler
    fun onGamePlayerDeath(e: PlayerDeathEvent) {
        if (!value) return

        val player = e.entity
        player.getGamePlayer() ?: return

        val spectator = Spectator(player.uniqueId, Spectator.Type.TEAM_ONLY)

        object : BukkitRunnable() {
            override fun run() {
                player.spigot().respawn()

                SpectatorManager.spectators[player.uniqueId] = spectator
                spectator.isSpectating = true
            }
        }.runTaskLater(UHC.instance, 7)
    }

}