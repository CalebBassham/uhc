package com.oblivionuhc.uhc.game.setting.time

import com.oblivionuhc.uhc.TimeExpression
import com.oblivionuhc.uhc.formatTicks
import com.oblivionuhc.uhc.game.setting.GameSetting

open class TimeGameSetting(name: String, defaultValue: Long, savable: Boolean) : GameSetting<Long>(name, defaultValue, savable) {

    override var value: Long = defaultValue

    fun setUsingTimeExpression(timeExpression: String) = setUsingTimeExpression(TimeExpression(timeExpression))

    fun setUsingTimeExpression(timeExpression: TimeExpression) {
        value = timeExpression.ticks
    }

    override fun displayValue(): String {
        return formatTicks(value)
    }

}