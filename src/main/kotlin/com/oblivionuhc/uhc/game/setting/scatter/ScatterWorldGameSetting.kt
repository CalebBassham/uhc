package com.oblivionuhc.uhc.game.setting.scatter

import com.oblivionuhc.uhc.exception.GameConfigurationException
import com.oblivionuhc.uhc.exception.GameSettingNotRegistered
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import com.oblivionuhc.uhc.operator.plus
import org.bukkit.ChatColor
import org.bukkit.World
import java.util.*

class ScatterWorldGameSetting : GameSetting<Optional<World>>("Scatter World", Optional.empty(), false) {

    override var value: Optional<World> = this.defaultValue
        set(newValue) {
            if (newValue.isPresent) {
                try {
                    val worlds = Game.mustGetSetting(WorldsGameSetting::class.java).value

                    if (!worlds.contains(newValue.get())) {
                        throw GameConfigurationException("The scatter world must be set to a game world.")
                    }
                } catch (e: GameSettingNotRegistered) {
                    throw GameConfigurationException("There was problem finding the game worlds.")
                }
            }

            field = newValue
        }
        get() {
            if (!field.isPresent) {
                val worlds = Game.mustGetSetting(WorldsGameSetting::class.java).value

                if (worlds.size == 1) return Optional.of(worlds.first())
            }

            return field
        }

    override fun displayValue(): String {
        if (!value.isPresent) {
            return ChatColor.ITALIC + "none"
        } else {
            return value.get().name
        }
    }
}