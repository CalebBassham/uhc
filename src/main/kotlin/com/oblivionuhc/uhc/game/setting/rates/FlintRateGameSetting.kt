package com.oblivionuhc.uhc.game.setting.rates

import com.oblivionuhc.uhc.chance
import com.oblivionuhc.uhc.dropItemFixed
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.PercentGameSetting
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.inventory.ItemStack

class FlintRateGameSetting : PercentGameSetting("Flint Rate", 0.1, true), Listener {

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(e: BlockBreakEvent) {
        if(e.block.type != Material.GRAVEL) return

        val world = e.block.world
        val location = e.block.location

        if (Game.getSetting(WorldsGameSetting::class.java)?.value?.contains(world) != true) return

        if (value == defaultValue) return

        if (!chance(value)) return

        e.isDropItems = false

        world.dropItemFixed(location, ItemStack(Material.FLINT))
    }

}