package com.oblivionuhc.uhc.game.setting.team

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.time.TimeGameSetting

class TeamspeakTimeGameSetting : TimeGameSetting("Teamspeak Time", 0, true) {

    override fun display() = Game.getSetting(TeamSizeGameSetting::class.java)?.display() ?: false
}