package com.oblivionuhc.uhc.game.setting

import com.oblivionuhc.uhc.getGamePlayer
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityRegainHealthEvent

class NaturalHealthRegenerationGameSettng : GameSetting<Boolean>("Natural Health Regeneration", false, true), Listener {

    @EventHandler(ignoreCancelled = true)
    fun onHealthRegen(e: EntityRegainHealthEvent) {
        if (value) return

        val entity = e.entity
        if (entity is Player && entity.getGamePlayer()?.alive == false) return

        if (e.regainReason == EntityRegainHealthEvent.RegainReason.REGEN || e.regainReason == EntityRegainHealthEvent.RegainReason.SATIATED) e.isCancelled = true
    }

}