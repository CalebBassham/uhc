package com.oblivionuhc.uhc.game.setting.world

import com.oblivionuhc.uhc.XZLocation
import com.oblivionuhc.uhc.game.setting.GameSetting

class MapCenterGameSetting : GameSetting<XZLocation>("Map Center", XZLocation(0.0, 0.0), false) {

    override fun displayValue(): String {
        return "x: ${value.x} z: ${value.z}"
    }

}