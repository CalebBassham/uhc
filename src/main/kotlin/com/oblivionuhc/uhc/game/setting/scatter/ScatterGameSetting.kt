package com.oblivionuhc.uhc.game.setting.scatter

import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.scatter.RandomScatter
import com.oblivionuhc.uhc.scatter.Scatter
import org.bukkit.plugin.java.JavaPlugin

class ScatterGameSetting(plugin: JavaPlugin) : GameSetting<Scatter>("Scatter", RandomScatter(plugin), false) {

    override fun displayValue() = value.name

}