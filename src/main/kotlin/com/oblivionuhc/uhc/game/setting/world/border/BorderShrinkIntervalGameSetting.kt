package com.oblivionuhc.uhc.game.setting.world.border

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.time.TimeGameSetting

class BorderShrinkIntervalGameSetting : TimeGameSetting("Border Shrink Interval", 20 * 90, true) {

    override fun display() = Game.getSetting(EndgameTypeGameSetting::class.java)?.let { it.value == EndgameType.BORDER_SHRINK }
        ?: false

}