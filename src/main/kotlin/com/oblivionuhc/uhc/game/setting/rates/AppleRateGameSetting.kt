package com.oblivionuhc.uhc.game.setting.rates

import com.oblivionuhc.uhc.chance
import com.oblivionuhc.uhc.dropItemFixed
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.PercentGameSetting
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.LeavesDecayEvent
import org.bukkit.inventory.ItemStack

class AppleRateGameSetting : PercentGameSetting("Apple Rate", 0.005, true), Listener {

    @EventHandler(ignoreCancelled = true)
    fun onLeaveDecay(e: LeavesDecayEvent) {
        val block = e.block
        if (block.type != Material.OAK_LEAVES && block.type != Material.DARK_OAK_LEAVES) return

        val world = block.world
        val location = block.location

        if (Game.getSetting(WorldsGameSetting::class.java)?.value?.contains(world) != true) return

        if (value == defaultValue) return

        if (!chance(value)) return

        e.isCancelled = true

        world.dropItemFixed(location, ItemStack(Material.APPLE))
    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(e: BlockBreakEvent) {
        val block = e.block
        if (block.type != Material.OAK_LEAVES && block.type != Material.DARK_OAK_LEAVES) return

        if (Game.getSetting(ShearsWorkGameSetting::class.java)?.value != true) return

        val world = e.block.world
        val location = e.block.location

        if (Game.getSetting(WorldsGameSetting::class.java)?.value?.contains(world) != true) return

        if (value == defaultValue) return

        if (!chance(value)) return

        e.isDropItems = false

        world.dropItemFixed(location, ItemStack(Material.APPLE))
    }

}