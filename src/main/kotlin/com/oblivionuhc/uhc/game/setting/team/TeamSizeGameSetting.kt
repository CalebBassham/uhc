package com.oblivionuhc.uhc.game.setting.team

import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.MinimumValueGameSetting
import me.calebbassham.scenariomanager.api.Scenario
import me.calebbassham.scenariomanager.api.TeamAssigner
import me.calebbassham.scenariomanager.api.scenarioManager

class TeamSizeGameSetting : GameSetting<Int>("Team Size", 1, false), MinimumValueGameSetting<Int> {

    override fun displayValue() =
        scenarioManager.scenarios
            .filter { it.isEnabled }
            .filterIsInstance<TeamAssigner>()
            .map { it as Scenario }
            .firstOrNull()
            ?.name
            ?: value.toString()

    override val minimumValue = 1
}