package com.oblivionuhc.uhc.game.setting.world

import com.oblivionuhc.uhc.game.setting.GameSetting

class MapRadiusGameSetting : GameSetting<Int>("Map Radius", 1500, true)