package com.oblivionuhc.uhc.game.setting.endGameType

import com.oblivionuhc.uhc.game.setting.GameSetting

class EndgameTypeGameSetting : GameSetting<EndgameType>("Endgame Type", EndgameType.BORDER_SHRINK, true) {

    override fun displayValue(): String {
        return super.displayValue().toLowerCase().replace("_", " ")
    }

}