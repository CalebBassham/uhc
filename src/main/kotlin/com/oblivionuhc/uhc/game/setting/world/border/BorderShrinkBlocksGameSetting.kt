package com.oblivionuhc.uhc.game.setting.world.border

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.MaximumValueGameSetting
import com.oblivionuhc.uhc.game.setting.MinimumValueGameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.FinalMapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.MapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkType
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkTypeGameSetting

class BorderShrinkBlocksGameSetting : GameSetting<Double>("Border Shrink Blocks", 50.0, true), MinimumValueGameSetting<Double>, MaximumValueGameSetting<Double> {

    override val minimumValue = 0.001

    override val maximumValue: Double
        get() {
            val mapRadiusGameSetting = Game.getSetting(MapRadiusGameSetting::class.java)
            val finalMapRadiusGameSetting = Game.getSetting(FinalMapRadiusGameSetting::class.java)

            if (mapRadiusGameSetting != null && finalMapRadiusGameSetting != null) {
                return (mapRadiusGameSetting.value - finalMapRadiusGameSetting.value).toDouble()
            }

            if (mapRadiusGameSetting != null && finalMapRadiusGameSetting == null) {
                return mapRadiusGameSetting.value - 50.0
            }

            return 100.0
        }

    override fun display(): Boolean {
        val endgameType = Game.getSetting(EndgameTypeGameSetting::class.java)?.value ?: return true
        val borderShrinkType = Game.getSetting(BorderShrinkTypeGameSetting::class.java)?.value ?: return true

        return endgameType == EndgameType.BORDER_SHRINK && borderShrinkType == BorderShrinkType.CONTINUOUS
    }
}