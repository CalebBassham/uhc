package com.oblivionuhc.uhc.game.setting.world.border

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.PercentGameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkType
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkTypeGameSetting

class BorderShrinkRateGameSetting : PercentGameSetting("Border Shrink Rate", 0.15, true) {

    override fun display(): Boolean {

        val borderShrinkType = Game.getSetting(BorderShrinkTypeGameSetting::class.java)?.value ?: return true
        val endGameType = Game.getSetting(EndgameTypeGameSetting::class.java)?.value ?: return true

        if (endGameType != EndgameType.BORDER_SHRINK || borderShrinkType != BorderShrinkType.EXPONENTIAL) {
            return false
        }

        return true
    }

}