package com.oblivionuhc.uhc.game.setting.world

import com.oblivionuhc.uhc.exception.GameSettingNotRegistered
import com.oblivionuhc.uhc.format
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterWorldGameSetting
import com.oblivionuhc.uhc.operator.plus
import org.bukkit.ChatColor
import org.bukkit.World
import java.util.*

class WorldsGameSetting : GameSetting<Set<World>>("Worlds", emptySet(), false) {

    override var value: Set<World> = defaultValue
        set(value) {
            try {
                // clear scatter world if it is no longer a game world.
                val scatterWorldGameSetting = Game.mustGetSetting(ScatterWorldGameSetting::class.java)
                val scatterWorld = scatterWorldGameSetting.value

                if (scatterWorld.isPresent && !value.contains(scatterWorld.get())) {
                    scatterWorldGameSetting.value = Optional.empty()
                }
            } catch (e: GameSettingNotRegistered) {

            }


            field = value
        }

    override fun displayValue(): String {
        return if (value.isEmpty()) {
            ChatColor.ITALIC + "none"
        } else {
            value.map { it.name }.format()
        }
    }

}