package com.oblivionuhc.uhc.game.setting.endGameType

enum class EndgameType {

    BORDER_SHRINK, MEETUP

}