package com.oblivionuhc.uhc.game.setting.world

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.MaximumValueGameSetting
import com.oblivionuhc.uhc.game.setting.MinimumValueGameSetting

class FinalMapRadiusGameSetting : GameSetting<Int>("Final Map Radius", 50, true), MinimumValueGameSetting<Int>, MaximumValueGameSetting<Int> {

    override val minimumValue = 1

    override val maximumValue = Game.getSetting(MapRadiusGameSetting::class.java)?.value ?: 1500
}