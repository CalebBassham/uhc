package com.oblivionuhc.uhc.game.setting.scoreboard

import com.oblivionuhc.uhc.game.setting.GameSetting
import org.bukkit.Bukkit
import org.bukkit.scoreboard.DisplaySlot
import org.bukkit.scoreboard.RenderType
import org.bukkit.scoreboard.Scoreboard

class TabHealthGameSetting(private val scoreboard: Scoreboard = Bukkit.getScoreboardManager().mainScoreboard) : GameSetting<TabHealthGameSetting.Type>("Tab Health", Type.HEARTS, true) {

    enum class Type {
        NONE,
        HEARTS,
        INTEGER,
    }

    fun registerObjective() {
        unregisterObjective() // Just in case

        val obj = scoreboard.registerNewObjective("TabHealth", "health", "TabHealth")
        obj.displaySlot = DisplaySlot.PLAYER_LIST

        Bukkit.getOnlinePlayers().forEach { obj.getScore(it.name).score = it.health.toInt() } // Update score

        obj.renderType = when (value) {
            Type.HEARTS -> RenderType.HEARTS
            Type.INTEGER -> RenderType.INTEGER
            else -> RenderType.HEARTS
        }

    }

    fun unregisterObjective() {
        scoreboard.getObjective("TabHealth")?.unregister()
    }

}