package com.oblivionuhc.uhc.game.setting

import java.text.NumberFormat

open class PercentGameSetting(name: String, default: Double, savable: Boolean) : GameSetting<Double>(name, default, savable), MinimumValueGameSetting<Double>, MaximumValueGameSetting<Double> {

    override val minimumValue = 0.0
    override val maximumValue = 1.0

    override fun displayValue(): String {
        return NumberFormat.getPercentInstance().format(value)
    }
}