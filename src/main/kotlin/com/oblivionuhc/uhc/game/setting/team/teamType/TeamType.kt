package com.oblivionuhc.uhc.game.setting.team.teamType

enum class TeamType {

    CHOSEN, RANDOM

}