package com.oblivionuhc.uhc.game.setting.world.border.outsideBorder

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.getGamePlayer
import com.oblivionuhc.uhc.getHighestTeleportable
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent

class OutsideBorderGameSetting : GameSetting<OutsideBorder>("Outside Border", OutsideBorder.TELEPORT, true), Listener {

    override fun displayValue(): String {
        return super.displayValue().toLowerCase().replace("_", " ")
    }

    override fun display() = Game.getSetting(EndgameTypeGameSetting::class.java)?.let { it.value == EndgameType.BORDER_SHRINK }
        ?: false

    @EventHandler
    fun onDamage(e: EntityDamageEvent) {
        if (value == OutsideBorder.TELEPORT) {
            val player = e.entity as? Player ?: return

            val gamePlayer = player.getGamePlayer() ?: return
            if (!gamePlayer.alive) {
                return
            }

            if (e.cause == EntityDamageEvent.DamageCause.SUFFOCATION) {
                if (handleBorderTeleport(player.world.worldBorder.size / 2, player)) {
                    e.isCancelled = true
                }
            }
        }
    }

    private fun handleBorderTeleport(newRadius: Double, player: Player): Boolean {
        val location = player.location

        var x: Double? = null
        var z: Double? = null

        if (location.x > newRadius) {
            x = newRadius - 0.5
        } else if (location.x < -newRadius) {
            x = -newRadius + 0.5
        }

        if (location.z > newRadius) {
            z = newRadius - 0.5
        } else if (location.z < -newRadius) {
            z = -newRadius + 0.5
        }

        if (x != null || z != null) {
            val newLocation = Location(player.world, x ?: location.blockX+0.5, 70.0, z
                ?: location.blockZ+0.5, location.yaw, location.pitch).getHighestTeleportable()
            player.teleport(newLocation)
            return true
        }

        return false
    }

}