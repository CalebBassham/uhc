package com.oblivionuhc.uhc.game.setting.world

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.weather.WeatherChangeEvent

class RainGameSetting : GameSetting<Boolean>("Rain", false, true), Listener {

    @EventHandler
    fun rain(e: WeatherChangeEvent) {
        if (!value) {
            Game.getSetting(WorldsGameSetting::class.java)?.let { setting ->
                if (setting.value.contains(e.world)) {
                    if (e.toWeatherState()) {
                        e.isCancelled = true
                    }
                }
            }
        }
    }
}