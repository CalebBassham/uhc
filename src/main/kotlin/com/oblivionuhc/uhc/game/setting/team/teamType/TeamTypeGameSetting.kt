package com.oblivionuhc.uhc.game.setting.team.teamType

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting

class TeamTypeGameSetting : GameSetting<TeamType>("Team Type", TeamType.CHOSEN, true) {

    override fun display() = Game.getSetting(TeamSizeGameSetting::class.java)?.let { it.value > 1 } == true
}