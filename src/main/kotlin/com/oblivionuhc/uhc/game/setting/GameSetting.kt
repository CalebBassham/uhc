package com.oblivionuhc.uhc.game.setting

abstract class GameSetting<T>(val name: String, val defaultValue: T, val savable: Boolean) {

    @Suppress("UNCHECKED_CAST")
    open var value: T = defaultValue
        set(value) {

            if (this is MinimumValueGameSetting<Number> || this is MaximumValueGameSetting<Number>) {
                if (this.value !is Number) {
                    throw RuntimeException("Cannot set RangeGameSetting to non-number value.")
                }

                if (this is MinimumValueGameSetting<Number>) {
                    if (value is Int) {
                        if (value < minimumValue.toInt()) {
                            field = minimumValue as T
                            return
                        }
                    }

                    if (value is Double) {
                        if (value < minimumValue.toDouble()) {
                            field = minimumValue as T
                            return
                        }
                    }

                    if (value is Long) {
                        if (value < minimumValue.toLong()) {
                            field = minimumValue as T
                            return
                        }
                    }
                }

                if (this is MaximumValueGameSetting<Number>) {
                    if (value is Int) {
                        if (value > maximumValue.toInt()) {
                            field = maximumValue as T
                            return
                        }
                    }

                    if (value is Double) {
                        if (value > maximumValue.toDouble()) {
                            field = maximumValue as T
                            return
                        }
                    }

                    if (value is Long) {
                        if (value > maximumValue.toLong()) {
                            field = maximumValue as T
                            return
                        }
                    }
                }
            }

            field = value
        }

    fun reset() {
        value = defaultValue
    }

    open fun displayValue(): String {
        return value.toString()
    }

    open fun display() = true

}