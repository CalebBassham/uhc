package com.oblivionuhc.uhc.game.setting

interface MinimumValueGameSetting<out T : Number> {
    val minimumValue: T
}