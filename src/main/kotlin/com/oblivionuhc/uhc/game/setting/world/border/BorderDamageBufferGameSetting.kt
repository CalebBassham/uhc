package com.oblivionuhc.uhc.game.setting.world.border

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.MinimumValueGameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorder
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorderGameSetting

class BorderDamageBufferGameSetting : GameSetting<Double>("Border Damage Buffer", 0.0, true), MinimumValueGameSetting<Double> {

    override val minimumValue = 0.0

    override fun display(): Boolean {
        val endgameType = Game.getSetting(EndgameTypeGameSetting::class.java)?.value ?: return true
        val outsideBorder = Game.getSetting(OutsideBorderGameSetting::class.java)?.value ?: return true

        return endgameType == EndgameType.BORDER_SHRINK && outsideBorder == OutsideBorder.DAMAGE || outsideBorder == OutsideBorder.KILL
    }

    override fun displayValue(): String {
        return "$value blocks"
    }

}