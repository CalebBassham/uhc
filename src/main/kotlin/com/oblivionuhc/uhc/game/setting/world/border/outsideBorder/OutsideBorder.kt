package com.oblivionuhc.uhc.game.setting.world.border.outsideBorder

enum class OutsideBorder {

    KILL, DAMAGE, TELEPORT

}