package com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting

class BorderShrinkTypeGameSetting : GameSetting<BorderShrinkType>("Border Shrink Type", BorderShrinkType.EXPONENTIAL, true) {

    override fun displayValue(): String {
        return super.displayValue().toLowerCase().replace("_", " ")
    }

    override fun display() = Game.getSetting(EndgameTypeGameSetting::class.java)?.let { it.value == EndgameType.BORDER_SHRINK }
        ?: false

}