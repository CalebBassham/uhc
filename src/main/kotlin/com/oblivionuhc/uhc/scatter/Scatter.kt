package com.oblivionuhc.uhc.scatter

import com.oblivionuhc.uhc.XZLocation
import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.scoreboard.Team
import java.util.concurrent.CompletableFuture

interface Scatter {
    val name: String

    fun onScatterPlayer(player: Player)
    fun onScatterTeam(team: Team)

    fun player(player: Player, world: World, center: XZLocation, radius: Int)
    fun team(team: Team, world: World, center: XZLocation, radius: Int)

    fun players(players: Collection<Player>, world: World, center: XZLocation, radius: Int, cooldown: Long = 0): CompletableFuture<Void>
    fun teams(teams: Collection<Team>, world: World, center: XZLocation, radius: Int, cooldown: Long = 0): CompletableFuture<Void>

    fun mixed(players: Collection<Player>, teams: Collection<Team>, world: World, center: XZLocation, radius: Int, cooldown: Long = 0): CompletableFuture<Void>
}