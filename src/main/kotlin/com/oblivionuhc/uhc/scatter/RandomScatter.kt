package com.oblivionuhc.uhc.scatter

import com.oblivionuhc.uhc.*
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.lang.broadcast
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.scoreboard.Team
import java.util.*
import java.util.concurrent.CompletableFuture

class RandomScatter(private val plugin: JavaPlugin) : Scatter {

    override val name = "Random"

    override fun onScatterPlayer(player: Player) {
        broadcast("player_scattered", mapOf(Pair("player", player.displayName)))
    }

    override fun onScatterTeam(team: Team) {
        broadcast("team_scattered", mapOf(Pair("team_name", team.name), Pair("team_players", team.players.map { it.getOfflinePlayerDisplayName() }.format())))
    }

    override fun player(player: Player, world: World, center: XZLocation, radius: Int) {
        goodScatterLocations(world, center, radius).whenComplete { locations, _ ->
            player.teleportAsync(locations.poll()).whenComplete { t, u ->
                onScatterPlayer(player)
            }

            Game.players.firstOrNull { it.player == player }?.scattered = true
        }
    }

    override fun team(team: Team, world: World, center: XZLocation, radius: Int) {
        goodScatterLocations(world, center, radius).whenComplete { locations, _ ->
            team.players.filter { it.isOnline }.forEach {
                val player = (it as Player)
                player.teleportAsync(locations.poll())
                Game.players.firstOrNull { it.player == player }?.scattered = true
            }
            onScatterTeam(team)
        }
    }

    override fun players(players: Collection<Player>, world: World, center: XZLocation, radius: Int, cooldown: Long): CompletableFuture<Void> {
        val future = CompletableFuture<Void>()
        val playersQ = LinkedList<Player>(players)

        goodScatterLocations(world, center, radius, players.size).whenComplete { locations, _ ->
            object : BukkitRunnable() {
                override fun run() {
                    val player = playersQ.poll()
                    val location = locations.poll()

                    player.teleportAsync(location)
                    onScatterPlayer(player)

                    Game.players.firstOrNull { it.player == player }?.scattered = true

                    if (playersQ.isEmpty() || locations.isEmpty()) {
                        cancel()
                        future.complete(null)
                    }
                }
            }.runTaskTimer(plugin, cooldown, cooldown)
        }

        return future
    }

    override fun teams(teams: Collection<Team>, world: World, center: XZLocation, radius: Int, cooldown: Long): CompletableFuture<Void> {
        val future = CompletableFuture<Void>()
        val teamsQ = LinkedList<Team>(teams)

        goodScatterLocations(world, center, radius, teams.size).whenComplete { locations, _ ->
            object : BukkitRunnable() {
                override fun run() {
                    val team = teamsQ.poll()
                    val location = locations.poll()

                    team.players.filter { it.isOnline }.forEach {
                        val player = (it as Player)
                        player.teleportAsync(location)

                        Game.players.firstOrNull { it.player == player }?.scattered = true
                    }
                    onScatterTeam(team)

                    if (teamsQ.isEmpty() || locations.isEmpty()) {
                        cancel()
                        future.complete(null)
                    }
                }
            }.runTaskTimer(plugin, cooldown, cooldown)
        }

        return future
    }

    override fun mixed(players: Collection<Player>, teams: Collection<Team>, world: World, center: XZLocation, radius: Int, cooldown: Long): CompletableFuture<Void> {
        val future = CompletableFuture<Void>()
        val playersQ = LinkedList<Player>(players)
        val teamsQ = LinkedList<Team>(teams)

        goodScatterLocations(world, center, radius, playersQ.size + teamsQ.size).whenComplete { locations, _ ->
            object : BukkitRunnable() {
                override fun run() {
                    if (!teamsQ.isEmpty()) {
                        val team = teamsQ.poll()
                        val location = locations.poll()

                        team.players.filter { it.isOnline }.forEach {
                            val player = (it as Player)
                            player.teleportAsync(location)

                            Game.players.firstOrNull { it.player == player }?.scattered = true
                        }

                        onScatterTeam(team)
                    } else if (!playersQ.isEmpty()) {
                        val player = playersQ.poll()
                        val location = locations.poll()

                        player.teleportAsync(location)
                        onScatterPlayer(player)

                        Game.players.firstOrNull { it.player == player }?.scattered = true
                    }

                    if (locations.isEmpty() && (playersQ.isEmpty() && teamsQ.isEmpty())) {
                        cancel()
                        future.complete(null)
                    }
                }
            }.runTaskTimer(plugin, cooldown, cooldown)
        }

        return future
    }

    private fun goodScatterLocations(world: World, center: XZLocation, radius: Int, amount: Int = 1, cooldown: Long = 20): CompletableFuture<Queue<Location>> {
        val future = CompletableFuture<Queue<Location>>()
        val locations = LinkedList<Location>()

        object : BukkitRunnable() {
            override fun run() {
                while (true) {
                    val coord = randomCoordinate(center, radius)
                    val location = Location(world, coord.x, 100.0, coord.z).getHighestTeleportable().centerOnBlock()

                    if (isGoodScatterLocation(location)) {
                        locations.add(location)
                        break
                    }
                }

                if (locations.size == amount) {
                    cancel()
                    future.complete(locations)
                }
            }
        }.runTaskTimer(plugin, 0, cooldown)

        return future
    }

    private fun isGoodScatterLocation(location: Location): Boolean {
        location.chunk.load()
        val block = location.clone().subtract(0.0, 1.0, 0.0).block

        val material = block.type

        return !block.isLiquid && !block.isEmpty && material.isSolid && (material.isOccluding || material == Material.SNOW) && material != Material.CACTUS && (material != Material.STONE || location.y >= 60) && location.y >= 40
    }

    private fun randomCoordinate(center: XZLocation, radius: Int) = XZLocation(randInt(center.x.toInt() - radius, center.x.toInt() + radius).toDouble(), randInt(center.z.toInt() - radius, center.z.toInt() + radius).toDouble())

}
