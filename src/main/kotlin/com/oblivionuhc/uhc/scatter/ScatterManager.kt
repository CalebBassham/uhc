package com.oblivionuhc.uhc.scatter

import com.oblivionuhc.uhc.exception.ScatterAlreadyRegisteredException
import java.util.*
import kotlin.collections.ArrayList

object ScatterManager {

    private val scatters = ArrayList<Scatter>()

    /**
     * Register a new scatter.
     * @param scatter The scatter to be registered.
     * @throws ScatterAlreadyRegisteredException
     */
    fun register(scatter: Scatter) {
        if (scatters.map { it.name }.contains(scatter.name)) {
            throw ScatterAlreadyRegisteredException(scatter)
        }

        scatters.add(scatter)
    }

    fun getScatters(): List<Scatter> {
        val copy = mutableListOf<Scatter>()
        Collections.copy(copy, scatters)
        return copy
    }

}