package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.setting.world.MapCenterGameSetting
import com.oblivionuhc.uhc.game.setting.world.MapRadiusGameSetting
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import me.calebbassham.scenariomanager.api.uhc.GameWorldProvider
import org.bukkit.Location
import org.bukkit.World

class ScenarioManagerGameWorldProvider : GameWorldProvider {

    override val gameWorlds: List<World>
        get() = Game.getSetting(WorldsGameSetting::class.java)?.value?.toList() ?: emptyList()


    override fun getMapCenter(world: World): Location? {
        val worlds = Game.getSetting(WorldsGameSetting::class.java)?.value ?: return null
        if (!worlds.contains(world)) return null

        return Game.getSetting(MapCenterGameSetting::class.java)?.value?.toLocation(world)
    }

    override fun getMapRadius(world: World): Int? {
        val worlds = Game.getSetting(WorldsGameSetting::class.java)?.value ?: return null
        if (!worlds.contains(world)) return null

        return Game.getSetting(MapRadiusGameSetting::class.java)?.value
    }
}