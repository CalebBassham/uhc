package com.oblivionuhc.uhc.scenarioManager

import me.calebbassham.scenariomanager.plugin.ScenarioManagerPlugin
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.server.PluginEnableEvent

class ScenarioManagerOnEnableListener : Listener {

    @EventHandler
    fun onScenarioManagerPluginEnable(e: PluginEnableEvent) {
        e.plugin as? ScenarioManagerPlugin ?: return
        setupScenarioManager()
    }

}