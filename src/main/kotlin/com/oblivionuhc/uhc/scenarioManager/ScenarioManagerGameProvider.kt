package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import me.calebbassham.scenariomanager.api.uhc.GameProvider

class ScenarioManagerGameProvider : GameProvider {

    override fun isGameRunning() = Game.state == GameState.RUNNING

}