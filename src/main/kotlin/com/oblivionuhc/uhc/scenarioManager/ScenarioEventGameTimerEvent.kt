package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.game.timer.GameTimer
import com.oblivionuhc.uhc.game.timer.event.GameTimerEvent
import me.calebbassham.scenariomanager.api.events.ScenarioEvent

class ScenarioEventGameTimerEvent(val scenarioEvent: ScenarioEvent, ticks: Long, fromStartOfGame: Boolean) : GameTimerEvent(scenarioEvent.name, if (fromStartOfGame) ticks else GameTimer.ticks + ticks, scenarioEvent.hide) {

    override fun run() {
        scenarioEvent.run()
    }

    override fun onTick(ticksRemaining: Long) {
        scenarioEvent.onTick(ticksRemaining)
    }


}