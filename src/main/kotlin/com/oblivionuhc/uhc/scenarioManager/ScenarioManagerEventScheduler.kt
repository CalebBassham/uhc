package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.game.timer.GameTimer
import me.calebbassham.scenariomanager.api.Scenario
import me.calebbassham.scenariomanager.api.events.ScenarioEvent
import me.calebbassham.scenariomanager.api.events.ScenarioEventScheduler

class ScenarioManagerEventScheduler : ScenarioEventScheduler {

    override val events: HashMap<ScenarioEvent, Long>
        get() {
            val events = HashMap<ScenarioEvent, Long>()

            for (scenarioEvent in GameTimer.events.filterIsInstance(ScenarioEventGameTimerEvent::class.java)) {
                events[scenarioEvent.scenarioEvent] = scenarioEvent.ticksRemaining
            }

            return events
        }

    override fun scheduleEvent(scenario: Scenario, event: ScenarioEvent, ticks: Long, fromStartOfGame: Boolean) {
        GameTimer.registerEvent(ScenarioEventGameTimerEvent(event, ticks, fromStartOfGame))
    }

    override fun removeScheduledEvents(scenario: Scenario) {

    }

}