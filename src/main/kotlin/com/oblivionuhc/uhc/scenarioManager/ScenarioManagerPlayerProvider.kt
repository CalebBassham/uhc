package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.game.Game
import me.calebbassham.scenariomanager.api.uhc.GamePlayerProvider
import org.bukkit.entity.Player

class ScenarioManagerGamePlayerProvider : GamePlayerProvider {

    override val gamePlayers: List<Player>
        get() = Game.players.filter { it.alive }.mapNotNull { it.player }

    override fun isGamePlayer(player: Player) = Game.players.firstOrNull { it.uuid == player.uniqueId } != null
}