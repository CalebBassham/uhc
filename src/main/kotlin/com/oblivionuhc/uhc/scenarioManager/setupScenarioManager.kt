package com.oblivionuhc.uhc.scenarioManager

import me.calebbassham.scenariomanager.api.scenarioManager

internal fun setupScenarioManager() {
    with(scenarioManager) {
        eventScheduler = ScenarioManagerEventScheduler()

        gamePlayerProvider = ScenarioManagerGamePlayerProvider()
        gameWorldProvider = ScenarioManagerGameWorldProvider()
        gameProvider = ScenarioManagerGameProvider()
        teamProvider = ScenarioManagerTeamProvider()
    }
}
