package com.oblivionuhc.uhc.scenarioManager

import com.oblivionuhc.uhc.teams.TeamManager
import me.calebbassham.scenariomanager.api.uhc.TeamProvider
import org.bukkit.ChatColor
import org.bukkit.OfflinePlayer
import org.bukkit.scoreboard.Team

class ScenarioManagerTeamProvider : TeamProvider {

    override fun getPlayerTeam(player: OfflinePlayer): Team? = TeamManager.scoreboard.getEntryTeam(player.name)

    override fun getTeam(name: String) = TeamManager.getTeam(name)


    override fun registerTeam(name: String?, color: ChatColor?): Team = TeamManager.create(name, color)

    override fun registerTeam(name: String?): Team = registerTeam(name, null)

    override fun registerTeam(): Team = registerTeam(null)


    override fun mustRegisterTeam(name: String?, color: ChatColor?) = registerTeam(name, color)

    override fun mustRegisterTeam(name: String?) = mustRegisterTeam(name, null)

    override fun mustRegisterTeam() = mustRegisterTeam(null)

}