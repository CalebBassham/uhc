package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.*
import co.aikar.commands.contexts.OnlinePlayer
import com.oblivionuhc.uhc.game.spectating.Spectator
import com.oblivionuhc.uhc.game.spectating.SpectatorManager
import com.oblivionuhc.uhc.game.spectating.SpectatorManager.getSpectator
import com.oblivionuhc.uhc.getGamePlayer
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("spectate|spec")
class SpecCmd : BaseCommand() {

    @CommandAlias("specchat|sc")
    @Subcommand("chat")
    @CommandPermission("uhc.spectate.chat.spectator")
    fun chat(sender: Player, message: String) {
        val spectator = sender.getSpectator()

        if (spectator == null) {
            LanguageManager.getMessage("not_a_spectator", sender).send(sender)
            return
        }

        spectator.sendChatMessage(message)
    }

    @Subcommand("add")
    @CommandPermission("uhc.spectate.others")
    @CommandCompletion("@players")
    fun add(sender: CommandSender, onlinePlayer: OnlinePlayer) {
        val player = onlinePlayer.player
        val gamePlayer = player.getGamePlayer()
        if (gamePlayer != null && gamePlayer.alive) {
            LanguageManager.getMessage("no_spectate_while_alive", sender).send(sender)
            return
        }

        if (SpectatorManager.spectators.containsKey(player.uniqueId)) {
            LanguageManager.getMessage("already_a_spectator", sender)
                ?.replaceVariable("player", player.displayName)
            return
        }

        SpectatorManager.spectators[player.uniqueId] = Spectator(player.uniqueId)

        LanguageManager.getMessage("add_spectator", player).send(player)

        LanguageManager.getMessage("add_spectator_player", sender)
            ?.replaceVariable("player", player.displayName)
            .send(sender)
    }

    @Subcommand("remove")
    @CommandPermission("uhc.spectate.others")
    @CommandCompletion("@players")
    fun remove(sender: CommandSender, onlinePlayer: OnlinePlayer) {
        val player = onlinePlayer.player
        val spectator = SpectatorManager.spectators.remove(player.uniqueId)

        if (spectator == null) {
            LanguageManager.getMessage("not_a_spectator_player", sender)
                ?.replaceVariable("player", player.displayName)
                .send(sender)
            return
        }

        spectator.isSpectating = false


        LanguageManager.getMessage("remove_spectator", player).send(player)

        LanguageManager.getMessage("remove_spectator_player", sender)
            ?.replaceVariable("player", player.displayName)
            .send(sender)
    }

    @Default
    @CatchUnknown
    fun toggleSelf(sender: Player, @Optional enable: Boolean?) {
        var spectator = sender.getSpectator()
        if (spectator == null) {
            if (!sender.hasPermission("uhc.spectate.self")) {
                throw InvalidCommandArgument("You are not a spectator for this game.", false)
            }

            val gamePlayer = sender.getGamePlayer()
            if (gamePlayer != null && gamePlayer.alive) {
                throw InvalidCommandArgument("An alive game player can not be added as a spectator.", false)
            }

            spectator = Spectator(sender.uniqueId)
            SpectatorManager.spectators[sender.uniqueId] = spectator
        }

        spectator.isSpectating = enable ?: !spectator.isSpectating
    }

}