package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.*
import com.oblivionuhc.uhc.Percentage
import com.oblivionuhc.uhc.TimeExpression
import com.oblivionuhc.uhc.XZLocation
import com.oblivionuhc.uhc.exception.GameConfigurationException
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.NaturalHealthRegenerationGameSettng
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.rates.AppleRateGameSetting
import com.oblivionuhc.uhc.game.setting.rates.FlintRateGameSetting
import com.oblivionuhc.uhc.game.setting.rates.ShearsWorkGameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterGameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterWorldGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamspeakTimeGameSetting
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamType
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamTypeGameSetting
import com.oblivionuhc.uhc.game.setting.time.EndgameTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.FinalHealTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.PvPTimeGameSetting
import com.oblivionuhc.uhc.game.setting.world.*
import com.oblivionuhc.uhc.game.setting.world.border.*
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkType
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorder
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorderGameSetting
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import com.oblivionuhc.uhc.scatter.ScatterManager
import org.bukkit.World
import org.bukkit.command.CommandSender
import java.util.Optional

@Suppress("UNUSED")
@CommandAlias("config|cfg")
class ConfigCmd : BaseCommand() {

    @Default
    fun config(sender: CommandSender) {
        Game.settings
            .filter { it.display() }
            .forEach {
                LanguageManager.getMessage("config", sender)
                    ?.replaceVariable("name", it.name)
                    ?.replaceVariable("value", it.displayValue())
                    .send(sender)
            }
    }

    @CommandPermission("uhc.config.reset")
    @Subcommand("reset")
    fun reset(sender: CommandSender) {
        Game.settings.forEach { it.reset() }
        LanguageManager.getMessage("config_reset_all", sender).send(sender)
    }

    @CommandPermission("uhc.config.reset")
    @Subcommand("reset")
    fun reset(sender: CommandSender, setting: GameSetting<*>) {
        if (Game.state != GameState.NOT_RUNNING) {
            LanguageManager.getMessage("alter_config_during_game", sender).send(sender)
            return
        }

        setting.reset()
        LanguageManager.getMessage("config_reset_one", sender)
            ?.replaceVariable("name", setting.name)
            ?.replaceVariable("default_value", setting.displayValue())
            .send(sender)
    }

    @CommandPermission("uhc.config.edit.worlds")
    @Subcommand("worlds")
    @CommandCompletion("@worlds")
    fun worlds(sender: CommandSender, worlds: Array<out World>) {
        if (worlds.isEmpty()) {
            LanguageManager.getMessage("specify_world", sender).send(sender)
            return
        }

        config(sender, WorldsGameSetting::class.java, worlds.toSet())
    }

    @CommandPermission("uhc.config.edit.map_radius")
    @Subcommand("map_radius")
    fun mapRadius(sender: CommandSender, radius: Int) {
        config(sender, MapRadiusGameSetting::class.java, radius)
    }

    @CommandPermission("uhc.config.edit.rain")
    @Subcommand("rain")
    fun rain(sender: CommandSender, boolean: Boolean) {
        config(sender, RainGameSetting::class.java, boolean)
    }

    @CommandPermission("uhc.config.edit.final_heal_time")
    @Subcommand("final_heal_time")
    fun finalHealTime(sender: CommandSender, time: TimeExpression) {
        config(sender, FinalHealTimeGameSetting::class.java, time)
    }

    @CommandPermission("uhc.config.edit.pvp_time")
    @Subcommand("pvp_time")
    fun pvpTime(sender: CommandSender, time: TimeExpression) {
        config(sender, PvPTimeGameSetting::class.java, time)
    }

    @CommandPermission("uhc.config.edit.border_shrink_type")
    @Subcommand("border_shrink_type")
    fun borderShrinkType(sender: CommandSender, type: BorderShrinkType) {
        config(sender, BorderShrinkTypeGameSetting::class.java, type)
    }

    @CommandPermission("uhc.config.edit.endgame_time")
    @Subcommand("endgame_time")
    fun endgameTime(sender: CommandSender, time: TimeExpression) {
        config(sender, EndgameTimeGameSetting::class.java, time)
    }

    @CommandPermission("uhc.config.edit.endgame_type")
    @Subcommand("endgame_type")
    fun endgameType(sender: CommandSender, type: EndgameType) {
        config(sender, EndgameTypeGameSetting::class.java, type)
    }

    @CommandPermission("uhc.config.edit.map_center")
    @Subcommand("map_center")
    fun mapCenter(sender: CommandSender, location: XZLocation) {
        config(sender, MapCenterGameSetting::class.java, location)
    }

    @CommandPermission("uhc.config.edit.final_map_radius")
    @Subcommand("final_map_radius")
    fun finalMapRadius(sender: CommandSender, radius: Int) {
        config(sender, FinalMapRadiusGameSetting::class.java, radius)
    }

    @CommandPermission("uhc.config.edit.border_shrink_rate")
    @Subcommand("border_shrink_rate")
    fun borderShrinkRate(sender: CommandSender, rate: Percentage) {
        config(sender, BorderShrinkRateGameSetting::class.java, rate.value)
    }

    @CommandPermission("uhc.config.edit.border_shrink_interval")
    @Subcommand("border_shrink_interval")
    fun borderShrinkInterval(sender: CommandSender, time: TimeExpression) {
        config(sender, BorderShrinkIntervalGameSetting::class.java, time)
    }

    @CommandPermission("uhc.config.edit.border_shrink_blocks")
    @Subcommand("border_shrink_blocks")
    fun borderShrinkBlocks(sender: CommandSender, blocks: Double) {
        config(sender, BorderShrinkBlocksGameSetting::class.java, blocks)
    }

    @CommandPermission("uhc.config.edit.outside_border")
    @Subcommand("outside_border")
    fun outsideBorder(sender: CommandSender, type: OutsideBorder) {
        config(sender, OutsideBorderGameSetting::class.java, type)
    }

    @CommandPermission("uhc.config.edit.border_damage")
    @Subcommand("border_damage")
    fun borderDamage(sender: CommandSender, damage: Double) {
        config(sender, BorderDamageGameSetting::class.java, damage)
    }

    @CommandPermission("uhc.config.edit.border_damage_buffer")
    @Subcommand("border_damage_buffer")
    fun borderDamageBuffer(sender: CommandSender, distance: Double) {
        config(sender, BorderDamageBufferGameSetting::class.java, distance)
    }

    @CommandPermission("uhc.config.edit.scatter")
    @Subcommand("scatter")
    fun scatter(sender: CommandSender, name: String) {
        config(sender, ScatterGameSetting::class.java, ScatterManager.getScatters().firstOrNull { it.name == name }
            ?: throw InvalidCommandArgument("That is not a valid scatter."))
    }

    @CommandPermission("uhc.config.edit.scatter_world")
    @Subcommand("scatter_world")
    fun scatterWorld(sender: CommandSender, world: World) {
        config(sender, ScatterWorldGameSetting::class.java, Optional.of(world))
    }

    @CommandPermission("uhc.config.edit.team_type")
    @Subcommand("team_type")
    fun teamType(sender: CommandSender, teamType: TeamType) {
        config(sender, TeamTypeGameSetting::class.java, teamType)
    }

    @CommandPermission("uhc.config.edit.team_size")
    @Subcommand("team_size")
    fun teamSize(sender: CommandSender, teamSize: Int) {
        config(sender, TeamSizeGameSetting::class.java, teamSize)
    }

    @CommandPermission("uhc.config.edit.teamspeak_time")
    @Subcommand("teamspeak_time")
    fun teamspeakTime(sender: CommandSender, time: TimeExpression) {
        config(sender, TeamspeakTimeGameSetting::class.java, time)
    }

    @CommandPermission("uhc.config.edit.natural_health_regeneration")
    @Subcommand("natural_health_regeneration")
    fun naturalRegeneration(sender: CommandSender, regen: Boolean) {
        config(sender, NaturalHealthRegenerationGameSettng::class.java, regen)
    }

    @CommandPermission("uhc.config.edit.flint_rate")
    @Subcommand("flint_rate")
    fun flintRate(sender: CommandSender, rate: Percentage) {
        config(sender, FlintRateGameSetting::class.java, rate.value)
    }

    @CommandPermission("uhc.config.edit.apple_rate")
    @Subcommand("apple_rate")
    fun appleRate(sender: CommandSender, rate: Percentage) {
        config(sender, AppleRateGameSetting::class.java, rate.value)
    }

    @CommandPermission("uhc.config.edit.shears_work")
    @Subcommand("shears_work")
    fun shearsWork(sender: CommandSender, shearsWork: Boolean) {
        config(sender, ShearsWorkGameSetting::class.java, shearsWork)
    }

    private fun <U : GameSetting<T>, T> config(sender: CommandSender, settingClass: Class<U>, value: T) {
        if (Game.state != GameState.NOT_RUNNING) {
            LanguageManager.getMessage("alter_config_during_game", sender).send(sender)
            return
        }

        val setting = Game.getSetting(settingClass)
            ?: throw InvalidCommandArgument("That cannot be set right now.", false)

        try {
            setting.value = value
        } catch (e: GameConfigurationException) {
            throw InvalidCommandArgument(e.message, false)
        }

        LanguageManager.getMessage("config_set_setting_value", sender)
            ?.replaceVariable("name", setting.name)
            ?.replaceVariable("value", setting.displayValue())
            .send(sender)
    }

    private fun <U : GameSetting<Long>> config(sender: CommandSender, settingClass: Class<U>, timeExpression: TimeExpression) {
        if (Game.state != GameState.NOT_RUNNING) {
            LanguageManager.getMessage("alter_config_during_game", sender).send(sender)
            return
        }

        val setting = Game.getSetting(settingClass)
            ?: throw InvalidCommandArgument("That cannot be set right now.", false)

        try {
            setting.value = timeExpression.ticks
        } catch (e: GameConfigurationException) {
            throw InvalidCommandArgument(e.message, false)
        }

        LanguageManager.getMessage("config_set_setting_value", sender)
            ?.replaceVariable("name", setting.name)
            ?.replaceVariable("value", setting.displayValue())
            .send(sender)
    }

}