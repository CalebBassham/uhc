package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.CommandPermission
import co.aikar.commands.annotation.Subcommand
import com.oblivionuhc.uhc.exception.GameConfigurationException
import com.oblivionuhc.uhc.exception.GameNotRunningException
import com.oblivionuhc.uhc.exception.GameRunningException
import com.oblivionuhc.uhc.exception.GameSetupException
import com.oblivionuhc.uhc.format
import com.oblivionuhc.uhc.formatTicks
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import com.oblivionuhc.uhc.game.timer.GameTimer
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.broadcast
import com.oblivionuhc.uhc.lang.send
import org.bukkit.World
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

@CommandAlias("ultrahardcore|uhc")
class UltraHardcoreCmd(private val plugin: JavaPlugin) : BaseCommand() {

    @Subcommand("start")
    @CommandPermission("uhc.command.ultrahardcore.start")
    fun start(sender: CommandSender, world: World) {
        try {
            Game.start(plugin)
        } catch (e: GameRunningException) {
            LanguageManager.getMessage("game_already_running", sender).send(sender)
            return
        } catch (e: GameConfigurationException) {
            throw InvalidCommandArgument(e.message, false)
        } catch (e: GameSetupException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("stop")
    @CommandPermission("uhc.command.ultrahardcore.stop")
    fun stop(sender: CommandSender) {
        try {
            Game.stop()
        } catch (e: GameNotRunningException) {
            LanguageManager.getMessage("no_game_running", sender).send(sender)
            return
        }

        broadcast("game_force_end")
    }

    @Subcommand("timers")
    @CommandAlias("timers")
    @CommandPermission("uhc.command.ultrahardcore.timers")
    fun timers(sender: CommandSender) {
        if (Game.state != GameState.RUNNING) {
            LanguageManager.getMessage("no_game_running", sender).send(sender)
            return
        }

        if (GameTimer.events.isEmpty()) {
            LanguageManager.getMessage("no_timers", sender).send(sender)
            return
        }

        GameTimer.events
            .filterNot { it.hide }
            .forEach {
                LanguageManager.getMessage("timer", sender)
                    ?.replaceVariable("timer_name", it.name)
                    ?.replaceVariable("time_remaining", formatTicks(it.ticksRemaining))
                    .send(sender)
            }
    }

    @Subcommand("players")
    fun players(sender: CommandSender) {
        if (Game.state != GameState.RUNNING) {
            LanguageManager.getMessage("no_game_running", sender).send(sender)
            return
        }

        LanguageManager.getMessage("list_game_players", sender)
            ?.replaceVariable("players", Game.players.map { it.name }.format())
            .send(sender)
    }
}