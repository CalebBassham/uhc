package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.*
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import org.bukkit.GameMode
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("gamemode|gm")
class GameModeCmd : BaseCommand() {

    @CommandCompletion("@players")
    @CommandAlias("creative|gmc")
    @Subcommand("creative|c")
    @CommandPermission("uhc.command.gamemode.creative")
    fun creative(sender: CommandSender, @Optional @Flags("other,defaultself") player: Player?) {
        gameMode(sender, player, GameMode.CREATIVE)
    }

    @CommandCompletion("@players")
    @CommandAlias("survival|gms")
    @Subcommand("survival|s")
    @CommandPermission("uhc.command.gamemode.survival")
    fun survival(sender: CommandSender, @Optional @Flags("other,defaultself") player: Player?) {
        gameMode(sender, player, GameMode.SURVIVAL)
    }

    @CommandCompletion("@players")
    @CommandAlias("adventure|gma")
    @Subcommand("adventure|a")
    @CommandPermission("uhc.command.gamemode.adventure")
    fun adventure(sender: CommandSender, @Optional @Flags("other,defaultself") player: Player?) {
        gameMode(sender, player, GameMode.ADVENTURE)
    }

    @CommandCompletion("@players")
    @Subcommand("spectator|spec")
    @CommandPermission("uhc.command.gamemode.spectator")
    fun spectator(sender: CommandSender, @Optional @Flags("other,defaultself") player: Player?) {
        gameMode(sender, player, GameMode.SPECTATOR)
    }

    private fun gameMode(sender: CommandSender, target: Player?, gameMode: GameMode) {
        target ?: return

        if (target.gameMode == gameMode) {
            throw InvalidCommandArgument((if (sender == target) "Your" else target.name + "'s") + " game mode is already " + target.gameMode.name.toLowerCase() + ".", false)
        }

        target.gameMode = gameMode
        LanguageManager.getMessage("game_mode", target)
            ?.replaceVariable("game_mode", target.gameMode.name.toLowerCase())
            .send(target)
        if (target != sender) {
            LanguageManager.getMessage("game_mode", sender)
                ?.replaceVariable("player", target.displayName)
                ?.replaceVariable("game_mode", target.gameMode.name.toLowerCase())
                .send(sender)
        }
    }

}