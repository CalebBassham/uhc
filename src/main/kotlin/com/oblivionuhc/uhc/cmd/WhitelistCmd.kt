package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.*
import com.oblivionuhc.uhc.format
import com.oblivionuhc.uhc.getOfflinePlayerDisplayName
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.broadcast
import com.oblivionuhc.uhc.lang.send
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.command.CommandSender

@CommandAlias("whitelist|wl")
@CommandPermission("uhc.command.whitelist")
class WhitelistCmd : BaseCommand() {

    @CommandPermission("uhc.command.whitelist.add")
    @Subcommand("add")
    @CommandCompletion("@unwhitelisted")
    fun add(sender: CommandSender, target: OfflinePlayer) {
        if (Bukkit.getWhitelistedPlayers().contains(target)) {
            LanguageManager.getMessage("already_whitelisted", sender)
                ?.replaceVariable("player", target.getOfflinePlayerDisplayName())
                .send(sender)
        }

        target.isWhitelisted = true

        val targetPlayer = target.player
        if (targetPlayer != null) {
            LanguageManager.getMessage("whitelisted", targetPlayer)
                ?.replaceVariable("player", targetPlayer.getOfflinePlayerDisplayName())
                .send(targetPlayer)
        }

        if (target != sender) {
            LanguageManager.getMessage("whitelisted_player", sender)
                ?.replaceVariable("player", target.getOfflinePlayerDisplayName())
                .send(sender)
        }
    }

    @CommandPermission("uhc.command.whitelist.remove")
    @Subcommand("remove")
    @CommandCompletion("@whitelisted")
    @CommandAlias("unwhitelist")
    fun remove(sender: CommandSender, target: OfflinePlayer) {

        if (!Bukkit.getWhitelistedPlayers().contains(target)) {
            LanguageManager.getMessage("not_whitelisted", sender)
                ?.replaceVariable("player", target.getOfflinePlayerDisplayName())
                .send(sender)
        }

        target.isWhitelisted = false

        if (target == sender) {
            LanguageManager.getMessage("unwhitelisted", sender).send(sender)

        } else {
            LanguageManager.getMessage("unwhitelisted_player", sender)
                ?.replaceVariable("player", target.getOfflinePlayerDisplayName())
                .send(sender)
        }
    }

    @CommandPermission("uhc.command.whitelist.list")
    @Subcommand("list")
    @Default
    fun list(sender: CommandSender) {
        if (Bukkit.getWhitelistedPlayers().isEmpty()) {
            LanguageManager.getMessage("no_whitelisted_players", sender).send(sender)
        } else {
            LanguageManager.getMessage("list_whitelisted_players", sender)
                ?.replaceVariable("players", Bukkit.getWhitelistedPlayers().map { it.getOfflinePlayerDisplayName() }.format())
                .send(sender)
        }
    }

    @CommandPermission("uhc.command.whitelist.clear")
    @Subcommand("clear")
    fun clear(sender: CommandSender) {
        if (Bukkit.getWhitelistedPlayers().isEmpty()) {
            LanguageManager.getMessage("no_whitelisted_players", sender).send(sender)
            return
        }

        broadcast("whitelist_clear")
        Bukkit.getWhitelistedPlayers().clear()
    }

    @CommandPermission("uhc.command.whitelist.all")
    @Subcommand("all|*")
    fun all() {
        if (Bukkit.getWhitelistedPlayers().containsAll(Bukkit.getOnlinePlayers())) {
            throw InvalidCommandArgument("All online players are whitelisted.", false)
        }

        Bukkit.getWhitelistedPlayers().addAll(Bukkit.getOnlinePlayers())
        broadcast("whitelist_all")
    }

}