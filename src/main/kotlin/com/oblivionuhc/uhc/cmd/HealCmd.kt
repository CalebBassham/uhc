package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.*
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("heal")
class HealCmd : BaseCommand() {

    @CommandPermission("uhc.command.heal")
    @CommandCompletion("@players")
    @Default
    @CatchUnknown
    fun onCommand(sender: CommandSender, @Flags("other,defaultself") @Optional player: Player?) {
        player ?: return
        player.health = player.maxHealth

        LanguageManager.getMessage("heal", player).send(player)
        if (player != sender) {
            LanguageManager.getMessage("heal_player", sender)
                ?.replaceVariable("player", player.displayName)
                .send(sender)
        }
    }

}