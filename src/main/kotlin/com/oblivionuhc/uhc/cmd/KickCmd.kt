package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.*
import co.aikar.commands.contexts.OnlinePlayer
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.broadcast
import com.oblivionuhc.uhc.lang.orUndefinedMessage
import com.oblivionuhc.uhc.lang.send
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender

@CommandAlias("kick")
class KickCmd : BaseCommand() {

    @CatchUnknown
    @CommandCompletion("@players")
    @CommandPermission("uhc.command.kick")
    fun default(sender: CommandSender, player: OnlinePlayer, @Optional reason: String?) {
        if (reason == null) {
            player.player.kickPlayer(LanguageManager.getMessage("kicked", player.player).orUndefinedMessage(player.player).message)
        } else {
            player.player.kickPlayer(LanguageManager.getMessage("kicked_with_reason", player.player)?.replaceVariable("reason", reason).orUndefinedMessage(player.player).message)
        }

        if (sender != player) {
            LanguageManager.getMessage("kicked_player", sender)
                ?.replaceVariable("player", player.player.name)
                .send(sender)
        }
    }

    @Subcommand("all|*")
    @CommandAlias("kickall")
    @CommandPermission("uhc.command.kick.all")
    fun all(@Optional reason: String?) {
        Bukkit.getOnlinePlayers().filter { !it.hasPermission("uhc.command.kick.all.immune") }.forEach {
            when (reason == null) {
                true -> it.kickPlayer(LanguageManager.getMessage("kick_all", it).orUndefinedMessage(it).message)
                false -> it.kickPlayer(LanguageManager.getMessage("kick_all_with_reason", it)?.replaceVariable("reason", reason!!).orUndefinedMessage(it).message)
            }
        }

        when (reason == null) {
            true -> broadcast("kick_all")
            false -> broadcast("kick_all_with_reason", mapOf(Pair("reason", reason!!)))
        }
    }

}