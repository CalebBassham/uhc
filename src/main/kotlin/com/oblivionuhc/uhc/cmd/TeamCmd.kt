package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.annotation.CommandAlias
import co.aikar.commands.annotation.Subcommand
import co.aikar.commands.contexts.OnlinePlayer
import com.oblivionuhc.uhc.exception.TeamException
import com.oblivionuhc.uhc.formatOwnership
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import com.oblivionuhc.uhc.game.setting.world.WorldsGameSetting
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import com.oblivionuhc.uhc.teams.TeamManager
import com.oblivionuhc.uhc.teams.TeamManager.acceptTeamInvite
import com.oblivionuhc.uhc.teams.TeamManager.getTeam
import com.oblivionuhc.uhc.teams.TeamManager.leaveTeam
import com.oblivionuhc.uhc.teams.TeamManager.sendTeamInfo
import com.oblivionuhc.uhc.teams.TeamManager.sendTeamInvite
import com.oblivionuhc.uhc.teams.TeamManager.sendTeamList
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("team")
class TeamCmd : BaseCommand() {

    @Subcommand("create")
    fun create(sender: Player) {
        if (!TeamManager.enabled) {
            throw InvalidCommandArgument("Team management is not enabled.")
        }

        if (Bukkit.getScoreboardManager().mainScoreboard.getPlayerTeam(sender) != null) {
            throw InvalidCommandArgument("You are already on a team.", false)
        }

        TeamManager.create(setOf(sender))

        LanguageManager.getMessage("create_and_join_team", sender).send(sender)
    }

    @Subcommand("invite")
    fun invite(sender: Player, playerToInvite: OnlinePlayer) {
        try {
            sender.sendTeamInvite(playerToInvite.player)
        } catch (e: TeamException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("accept")
    fun accept(sender: Player, @co.aikar.commands.annotation.Optional invitingPlayer: OnlinePlayer?) {
        try {
            sender.acceptTeamInvite(invitingPlayer?.player)
        } catch (e: TeamException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("leave")
    fun leave(sender: Player) {
        try {
            sender.leaveTeam()
        } catch (e: TeamException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("reset")
    fun reset() {
        if (Game.state != GameState.NOT_RUNNING) {
            throw InvalidCommandArgument("You cannot reset team management right now.", false)
        }

        TeamManager.reset()
    }

    @Subcommand("list")
    fun list(sender: CommandSender) {
        try {
            sender.sendTeamList()
        } catch (e: TeamException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("info")
    fun info(sender: CommandSender, player: OnlinePlayer) {
        try {
            sender.sendTeamInfo(player.player)
        } catch (e: TeamException) {
            throw InvalidCommandArgument(e.message, false)
        }
    }

    @Subcommand("color")
    fun color(sender: CommandSender) {
        if (!TeamManager.enabled) {
            throw InvalidCommandArgument("Team management is disabled.", false)
        }

        for (team in TeamManager.getTeams()) {
            team.color = TeamManager.getNewTeamColor()
        }

        LanguageManager.getMessage("color_teams", sender).send(sender)
    }

    @Subcommand("setcolor")
    fun setColor(sender: CommandSender, teamName: String, color: ChatColor) {
        val team = TeamManager.getTeam(teamName) ?: throw InvalidCommandArgument("That is not a valid team number.")

        if (!TeamManager.teamColors.contains(color)) {
            LanguageManager.getMessage("invalid_team_color", sender).send(sender)
            return
        }

        team.color = color

        LanguageManager.getMessage("set_team_color", sender)
            ?.replaceVariable("name", team.name.formatOwnership())
            ?.replaceVariable("color", team.color.toString())
    }

    @Subcommand("chat")
    @CommandAlias("teamchat|tc|pm")
    fun chat(sender: Player, message: String) {
        val team = sender.getTeam() ?: throw InvalidCommandArgument("You are not on a team.", false)

        team.entries.mapNotNull { Bukkit.getPlayer(it) }.forEach {
            LanguageManager.getMessage("team_chat", it)
                ?.replaceVariable("sender", sender.displayName)
                ?.replaceVariable("team_color", team.color.toString())
                ?.replaceVariable("team_name", team.name)
                ?.replaceVariable("message", message)
                .send(it)
        }
    }

    @CommandAlias("pmcoords|teamlocation|tl")
    fun location(sender: Player) {
        val team = sender.getTeam() ?: throw InvalidCommandArgument("You are not on a team.", false)
        val location = sender.location
        val worlds = Game.getSetting(WorldsGameSetting::class.java)?.value ?: emptySet()

        if (!worlds.contains(location.world)) {
            throw InvalidCommandArgument("You are not in a game world.")
        }

        team.entries.mapNotNull { Bukkit.getPlayer(it) }.forEach {
            LanguageManager.getMessage("team_pmcoords", it)
                ?.replaceVariable("sender", it.displayName)
                ?.replaceVariable("team_color", team.color.toString())
                ?.replaceVariable("team_name", team.name)
                ?.replaceVariable("x", it.location.blockX.toString())
                ?.replaceVariable("y", it.location.blockY.toString())
                ?.replaceVariable("z", it.location.blockZ.toString())
                .send(it)
        }
    }

}