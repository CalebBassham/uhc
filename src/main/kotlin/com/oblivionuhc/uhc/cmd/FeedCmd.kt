package com.oblivionuhc.uhc.cmd

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.*
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.send
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("feed")
class FeedCmd : BaseCommand() {

    @CommandPermission("uhc.command.feed")
    @CatchUnknown
    @Default
    @CommandCompletion("@players")
    fun onCommand(sender: CommandSender, @Flags("other,defaultself") @Optional player: Player?) {
        player ?: return

        player.foodLevel = 20
        player.saturation = 20f
        player.exhaustion = 0f

        LanguageManager.getMessage("fed", player).send(player)

        if (player != sender) {
            LanguageManager.getMessage("fed_player", sender)
                ?.replaceVariable("player", player.displayName)
                .send(sender)
        }
    }

}