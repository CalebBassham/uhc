package com.oblivionuhc.uhc

import java.util.regex.Pattern

class TimeExpression(parse: String) {

    val ticks: Long

    init {
        val pattern = Pattern.compile("(?:(?<hours>\\d+)h)?(?:(?<minutes>\\d+)m)?(?:(?<seconds>\\d+)s)?")
        val matcher = pattern.matcher(parse)

        matcher.find()

        if (!matcher.matches()) {
            throw IllegalArgumentException("Not a valid time expression. You must follow the pattern \"" + pattern + "\"")
        }

        var ticks: Long = 0

        ticks += (matcher.group("hours") ?: "0").toLong() * 20 * 60 * 60
        ticks += (matcher.group("minutes") ?: "0").toLong() * 20 * 60
        ticks += (matcher.group("seconds") ?: "0").toLong() * 20

        this.ticks = ticks
    }

}