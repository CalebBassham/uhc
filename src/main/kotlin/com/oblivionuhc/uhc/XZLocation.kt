package com.oblivionuhc.uhc

import org.bukkit.Location
import org.bukkit.World

data class XZLocation(val x: Double, val z: Double) {

    fun toLocation(world: World, y: Double = 0.0): Location = Location(world, x, y, z)

}