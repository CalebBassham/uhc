package com.oblivionuhc.uhc

data class Percentage(val value: Double)