package com.oblivionuhc.uhc.freeze

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Monster
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.entity.EntityDamageByBlockEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.ProjectileLaunchEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.plugin.java.JavaPlugin

/**
 * Features:
 *  - No movement on x or z
 *  - No damage done to players
 *  - No damage taken from blocks, when damage would be taken the block is removed
 *  - No damage from entities, when damage would be taken the entity is removed if it is not a player
 *  - No blocks placed
 *  - No blocks broken
 *  - No projectiles launched
 *  - No interaction
 */
class DefaultFreezer(private val plugin: JavaPlugin) : Freezer, Listener {

    private val frozenPlayers = HashSet<OfflinePlayer>()

    override fun onEnable() {
        Bukkit.getPluginManager().registerEvents(this, plugin)
    }

    override fun onDisable() {
        HandlerList.unregisterAll(this)
    }

    override fun freeze(player: OfflinePlayer) {
        frozenPlayers.add(player)
    }

    override fun thaw(player: OfflinePlayer) {
        frozenPlayers.remove(player)
    }

    override fun getFrozenPlayers() = frozenPlayers.toTypedArray()
    override fun isFrozen(player: OfflinePlayer) = getFrozenPlayers().contains(player)

    @EventHandler(ignoreCancelled = true)
    fun onMove(e: PlayerMoveEvent) {
        if (!isFrozen(e.player)) {
            return
        }

        if (e.from.blockX != e.to.blockX) e.to.x = e.from.x

        if (e.from.blockZ != e.to.blockZ) e.to.z = e.from.z
    }

    @EventHandler(ignoreCancelled = true)
    fun onDamageByBlock(e: EntityDamageByBlockEvent) {
        val player = e.entity as? Player ?: return

        if (!isFrozen(player)) {
            return
        }

        e.isCancelled = true
        e.damager.type = Material.AIR
    }

    @EventHandler(ignoreCancelled = true)
    fun onDamageByEntity(e: EntityDamageByEntityEvent) {
        val player = e.entity as? Player ?: return

        if (!isFrozen(player)) {
            return
        }

        if (e.damager is Monster) {
            e.damager.remove()
        }


        e.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onDamage(e: EntityDamageEvent) {
        val player = e.entity as? Player ?: return

        if (!isFrozen(player)) {
            return
        }

        e.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onDamageToEntity(e: EntityDamageByEntityEvent) {
        val player = e.damager as? Player ?: return

        if (!isFrozen(player)) {
            return
        }

        if (e.entity is Player) {
            e.isCancelled = true
        }

    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockPlace(e: BlockPlaceEvent) {
        if (!isFrozen(e.player)) {
            return
        }

        e.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onBlockBreak(e: BlockBreakEvent) {
        if (!isFrozen(e.player)) {
            return
        }

        e.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onProjectileLaunch(e: ProjectileLaunchEvent) {
        val player = e.entity as? Player ?: return

        if (!isFrozen(player)) {
            return
        }

        e.isCancelled = true
    }

    @EventHandler(ignoreCancelled = true)
    fun onInteract(e: PlayerInteractEvent) {
        if (!isFrozen(e.player)) {
            return
        }

        e.isCancelled = true
    }

}