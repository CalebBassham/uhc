package com.oblivionuhc.uhc.freeze

import com.oblivionuhc.uhc.UHC

var freezer: Freezer = DefaultFreezer(UHC.instance).apply { onEnable() }
    set(value) {
        // get frozen Players
        // thaw all with old freezer
        // disable old freezer
        // enable new freezer
        // freeze with new freezer

        val frozenPlayers = field.getFrozenPlayers()
        field.thawAll()
        field.onDisable()


        field = value
        value.onEnable()
        frozenPlayers.forEach { value.freeze(it) }
    }