package com.oblivionuhc.uhc.freeze

import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer

interface Freezer {

    fun onEnable()
    fun onDisable()

    fun freeze(player: OfflinePlayer)
    fun thaw(player: OfflinePlayer)

    fun freezeAll() {
        Bukkit.getOnlinePlayers().forEach { freeze(it) }
    }

    fun thawAll() {
        Bukkit.getOnlinePlayers().forEach { thaw(it) }
    }

    fun getFrozenPlayers(): Array<OfflinePlayer>
    fun isFrozen(player: OfflinePlayer): Boolean

}