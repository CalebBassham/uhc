package com.oblivionuhc.uhc.exception

class GameNotRunningException : IllegalStateException("There is not a game running.")