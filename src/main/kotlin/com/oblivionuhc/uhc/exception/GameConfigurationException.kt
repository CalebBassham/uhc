package com.oblivionuhc.uhc.exception

class GameConfigurationException(message: String) : RuntimeException(message)