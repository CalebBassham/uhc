package com.oblivionuhc.uhc.exception

class GameTimerRunningException : RuntimeException("The game timer is currently running.")