package com.oblivionuhc.uhc.exception

import com.oblivionuhc.uhc.scatter.Scatter

class ScatterAlreadyRegisteredException(scatter: Scatter) : RuntimeException("The scatter \"$scatter\" is already registered.") {
}