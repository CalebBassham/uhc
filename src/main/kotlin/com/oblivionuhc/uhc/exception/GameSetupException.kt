package com.oblivionuhc.uhc.exception

class GameSetupException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)