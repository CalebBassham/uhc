package com.oblivionuhc.uhc.exception

class GameSettingNotRegistered(clazz: Class<*>) : RuntimeException("Game Setting \"${clazz.name}\" not registered.")