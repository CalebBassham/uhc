package com.oblivionuhc.uhc.exception

class GameRunningException : IllegalStateException("A game is running.")