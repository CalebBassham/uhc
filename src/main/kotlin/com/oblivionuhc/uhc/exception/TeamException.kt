package com.oblivionuhc.uhc.exception

class TeamException(message: String, reason: Throwable? = null) : RuntimeException(message, reason)