package com.oblivionuhc.uhc.exception

import org.bukkit.OfflinePlayer

class PlayerOfflineException(player: OfflinePlayer) : RuntimeException(String.format("The player \"%s\" is offline.", player.name
    ?: player.uniqueId))