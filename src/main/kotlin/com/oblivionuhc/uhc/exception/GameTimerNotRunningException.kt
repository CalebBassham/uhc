package com.oblivionuhc.uhc.exception

class GameTimerNotRunningException : RuntimeException("The game timer is not currently running.")