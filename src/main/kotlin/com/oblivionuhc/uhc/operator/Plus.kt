package com.oblivionuhc.uhc.operator

import org.bukkit.ChatColor

operator fun ChatColor.plus(chatColor: ChatColor): String {
    return this.toString() + chatColor
}

operator fun ChatColor.plus(string: String): String {
    return this.toString() + string
}

operator fun ChatColor.plus(number: Number): String {
    return this.toString() + number.toString()
}