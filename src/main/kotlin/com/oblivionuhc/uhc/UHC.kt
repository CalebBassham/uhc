package com.oblivionuhc.uhc

import co.aikar.commands.BukkitCommandManager
import co.aikar.commands.InvalidCommandArgument
import co.aikar.commands.PaperCommandManager
import com.oblivionuhc.uhc.cmd.*
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import com.oblivionuhc.uhc.game.human.GamePlayer
import com.oblivionuhc.uhc.game.setting.GameSetting
import com.oblivionuhc.uhc.game.setting.NaturalHealthRegenerationGameSettng
import com.oblivionuhc.uhc.game.setting.SpectateTeamatesAfterDeathGameSetting
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameType
import com.oblivionuhc.uhc.game.setting.endGameType.EndgameTypeGameSetting
import com.oblivionuhc.uhc.game.setting.rates.AppleRateGameSetting
import com.oblivionuhc.uhc.game.setting.rates.FlintRateGameSetting
import com.oblivionuhc.uhc.game.setting.rates.ShearsWorkGameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterGameSetting
import com.oblivionuhc.uhc.game.setting.scatter.ScatterWorldGameSetting
import com.oblivionuhc.uhc.game.setting.scoreboard.TabHealthGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting
import com.oblivionuhc.uhc.game.setting.team.TeamspeakTimeGameSetting
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamTypeGameSetting
import com.oblivionuhc.uhc.game.setting.time.EndgameTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.FinalHealTimeGameSetting
import com.oblivionuhc.uhc.game.setting.time.PvPTimeGameSetting
import com.oblivionuhc.uhc.game.setting.world.*
import com.oblivionuhc.uhc.game.setting.world.border.*
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkType
import com.oblivionuhc.uhc.game.setting.world.border.borderShrinkType.BorderShrinkTypeGameSetting
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorder
import com.oblivionuhc.uhc.game.setting.world.border.outsideBorder.OutsideBorderGameSetting
import com.oblivionuhc.uhc.game.spectating.SpectatorManager
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.scatter.RandomScatter
import com.oblivionuhc.uhc.scatter.ScatterManager
import com.oblivionuhc.uhc.scenarioManager.ScenarioManagerOnEnableListener
import com.oblivionuhc.uhc.scenarioManager.setupScenarioManager
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.World
import org.bukkit.plugin.java.JavaPlugin
import java.lang.NumberFormatException
import java.util.*
import java.util.logging.Logger
import java.util.regex.Pattern

class UHC : JavaPlugin() {

    companion object {
        internal lateinit var logger: Logger
        internal lateinit var instance: UHC
    }

    internal var isUsingScenarioManager = false

    override fun onEnable() {
        instance = this
        UHC.logger = logger

        if (Bukkit.getPluginManager().getPlugin("ScenarioManager") != null) {
            logger.info("Using ScenarioManager!")

            isUsingScenarioManager = true
            setupScenarioManager()
            Bukkit.getPluginManager().registerEvents(ScenarioManagerOnEnableListener(), this)
        } else {
            logger.info("Not using scenario manager.")
        }

        LanguageManager.load(this)

        Bukkit.getPluginManager().registerEvents(SpectatorManager, this)

        // setup command handler
        setupCommands()

        // setup Game object
        setupGame()
    }

    override fun onDisable() {
        if (Game.state == GameState.RUNNING) {
            Game.stop()
        }

        SpectatorManager.reset()
    }

    private fun setupCommands() {
        val manager = PaperCommandManager(this)

        registerCommandContexts(manager)
        registerCommandCompletions(manager)
        registerCommands(manager)
    }

    private fun registerCommandContexts(manager: BukkitCommandManager) { // alphabetize
        manager.commandContexts.registerContext(Array<out World>::class.java, { c ->
            val worlds = HashSet<World>()

            while (c.args.isNotEmpty()) {
                worlds.add(Bukkit.getWorld(c.popFirstArg())
                    ?: throw InvalidCommandArgument("That world does not exist or is not loaded.", false))
            }

            worlds.toArray(arrayOf<World>())
        })
        manager.commandContexts.registerContext(BorderShrinkType::class.java, { c ->
            try {
                BorderShrinkType.valueOf(c.popFirstArg().toUpperCase())
            } catch (e: IllegalArgumentException) {
                throw InvalidCommandArgument("That is not a valid border shrink type.")
            }
        })
        manager.commandContexts.registerContext(EndgameType::class.java, { c ->
            try {
                EndgameType.valueOf(c.popFirstArg().toUpperCase())
            } catch (e: IllegalArgumentException) {
                throw InvalidCommandArgument("That is not a valid endgame type.")
            }
        })
        manager.commandContexts.registerContext(GamePlayer::class.java, { c -> Game.players.firstOrNull { it.name.equals(c.popFirstArg(), ignoreCase = true) } })
        manager.commandContexts.registerContext(GameSetting::class.java, { c -> Game.getSetting(c.popFirstArg().replace("_", " ")).orElseThrow { InvalidCommandArgument("That is not a valid game setting.", false) } })
        manager.commandContexts.registerContext(OfflinePlayer::class.java, { Bukkit.getOfflinePlayer(it.popFirstArg()) })
        manager.commandContexts.registerContext(OutsideBorder::class.java, { c ->
            try {
                OutsideBorder.valueOf(c.popFirstArg().toUpperCase())
            } catch (e: IllegalArgumentException) {
                throw InvalidCommandArgument("That is not a valid outside border option.")
            }
        })
        manager.commandContexts.registerContext(Percentage::class.java, { c ->
            val pattern = Pattern.compile("(?<percent>\\d{0,3}(?:.\\d+)?)%?")
            val matcher = pattern.matcher(c.popFirstArg())

            matcher.find()

            if (!matcher.matches()) {
                throw InvalidCommandArgument("That is not a valid percent. Example: 12.5%")
            }

            Percentage(matcher.group("percent").toDouble() / 100)
        })
        manager.commandContexts.registerContext(TimeExpression::class.java, { c ->
            try {
                TimeExpression(c.popFirstArg())
            } catch (e: IllegalArgumentException) {
                throw InvalidCommandArgument("That is not a valid time expression. Example: 30m20s", false)
            }
        })
        manager.commandContexts.registerContext(XZLocation::class.java, { c ->
            val split = c.popFirstArg().split(",")

            if (split.size != 2) {
                throw InvalidCommandArgument("Incorrect format. Example: x,z")
            }

            val x: Double
            val z: Double
            try {
                x = split[0].toDouble()
                z = split[1].toDouble()
            } catch (e: NumberFormatException) {
                throw InvalidCommandArgument("Coordinates bust me numbers.")
            }

            XZLocation(x, z)
        })
    }

    private fun registerCommandCompletions(manager: BukkitCommandManager) { // alphabetize
        manager.commandCompletions.registerCompletion("alive", { Game.players.filter { it.alive }.map { it.name } })
        manager.commandCompletions.registerCompletion("dead", { Game.players.filter { !it.alive }.map { it.name } })
        manager.commandCompletions.registerCompletion("game", { Game.players.map { it.name } })
        manager.commandCompletions.registerCompletion("unwhitelisted", { Bukkit.getOnlinePlayers().filter { !it.isWhitelisted }.map { it.name } })
        manager.commandCompletions.registerCompletion("whitelisted", { Bukkit.getWhitelistedPlayers().map { it.name } })
//        manager.commandCompletions.registerCompletion("settings", { Game.settings.map { it.name.replace(" ", "_").toLowerCase() }})
    }

    private fun registerCommands(manager: BukkitCommandManager) { // alphabetize
        manager.registerCommand(ConfigCmd())
        manager.registerCommand(GameModeCmd())
        manager.registerCommand(FeedCmd())
        manager.registerCommand(HealCmd())
        manager.registerCommand(KickCmd())
        manager.registerCommand(TeamCmd())
        manager.registerCommand(UltraHardcoreCmd(this))
        manager.registerCommand(WhitelistCmd())
        manager.registerCommand(SpecCmd())
    }

    private fun setupGame() {
        Bukkit.getPluginManager().registerEvents(Game, this)
        registerGameSettings()
        registerDefaultScatters()
    }

    private fun registerDefaultScatters() {
        ScatterManager.register(RandomScatter(this))
    }

    private fun registerGameSettings() { // alphabetize
        Game.apply {
            val plugin = this@UHC
            registerSetting(AppleRateGameSetting(), plugin)
            registerSetting(BorderDamageBufferGameSetting(), plugin)
            registerSetting(BorderDamageGameSetting(), plugin)
            registerSetting(BorderShrinkBlocksGameSetting(), plugin)
            registerSetting(BorderShrinkIntervalGameSetting(), plugin)
            registerSetting(BorderShrinkRateGameSetting(), plugin)
            registerSetting(BorderShrinkTypeGameSetting(), plugin)
            registerSetting(EndgameTimeGameSetting(), plugin)
            registerSetting(EndgameTypeGameSetting(), plugin)
            registerSetting(FinalHealTimeGameSetting(), plugin)
            registerSetting(FinalMapRadiusGameSetting(), plugin)
            registerSetting(FlintRateGameSetting(), plugin)
            registerSetting(NaturalHealthRegenerationGameSettng(), plugin)
            registerSetting(MapCenterGameSetting(), plugin)
            registerSetting(MapRadiusGameSetting(), plugin)
            registerSetting(OutsideBorderGameSetting(), plugin)
            registerSetting(PvPTimeGameSetting(), plugin)
            registerSetting(RainGameSetting(), plugin)
            registerSetting(ScatterGameSetting(plugin), plugin)
            registerSetting(ScatterWorldGameSetting(), plugin)
            registerSetting(ShearsWorkGameSetting(), plugin)
            registerSetting(SpectateTeamatesAfterDeathGameSetting(), plugin)
            registerSetting(TabHealthGameSetting(), plugin)
            registerSetting(TeamSizeGameSetting(), plugin)
            registerSetting(TeamspeakTimeGameSetting(), plugin)
            registerSetting(TeamTypeGameSetting(), plugin)
            registerSetting(WorldsGameSetting(), plugin)
        }
    }

}