package com.oblivionuhc.uhc.event

import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.HandlerList

class BorderShrinkEvent(val newRadius: Double) : Event(), Cancellable {

    private var cancelled = false

    companion object {
        @JvmStatic
        val handlerList = HandlerList()
    }

    override fun getHandlers(): HandlerList {
        return handlerList
    }

    override fun setCancelled(cancel: Boolean) {
        cancelled = cancel
    }

    override fun isCancelled() = cancelled
}