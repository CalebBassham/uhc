package com.oblivionuhc.uhc

import com.comphenix.protocol.ProtocolLibrary
import com.comphenix.protocol.events.PacketContainer
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.human.GamePlayer
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.OfflinePlayer
import org.bukkit.World
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.util.Vector
import java.lang.reflect.InvocationTargetException
import java.util.concurrent.ThreadLocalRandom
import java.util.logging.Level

fun Location.getNearbyBlocks(radius: Int): Set<Block> {
    val blocks = HashSet<Block>()
    for (x in this.blockX - radius..this.blockX + radius) {
        for (y in this.blockY - radius..this.blockY + radius) {
            for (z in this.blockZ - radius..this.blockZ + radius) {
                blocks.add(this.world.getBlockAt(x, y, z))
            }
        }
    }

    return blocks
}

fun <E> Collection<E>.format(): String {
    val list = this.toList()
    val sb = StringBuilder()

    for (i in 0 until this.size) {
        sb.append(list[i])

        if (i == list.size - 1) {
            continue
        } else if (i == list.size - 2) {
            sb.append(" & ")
        } else {
            sb.append(", ")
        }
    }

    return sb.toString()
}

fun formatTicks(ticks: Long): String {
    var t = ticks

    val ticksPerSecond = 20
    val ticksPerMinute = ticksPerSecond * 60
    val ticksPerHour = ticksPerMinute * 60

    val hours = t / ticksPerHour
    t %= ticksPerHour

    val minutes = t / ticksPerMinute
    t %= ticksPerMinute

    val seconds = t / ticksPerSecond

    val sb = StringBuilder()

    if (hours > 0) {
        sb.append(hours.toString() + "h")
    }

    if (minutes > 0) {
        sb.append(minutes.toString() + "m")
    }

    if (seconds > 0 || sb.isEmpty()) {
        sb.append(seconds.toString() + "s")
    }

    return sb.toString()
}

fun Location.getHighestTeleportable(): Location =
    Location(this.world, this.x, this.world.getHighestBlockAt(this).location.y, this.z, this.yaw, this.pitch)

fun Location.centerOnBlock(): Location = this.clone().add(0.5, 0.5, 0.5)

fun Player.reset() {
    saturation = 20f
    foodLevel = 20
    exhaustion = 0f
    exp = 0f
    flySpeed = 0.1f
    walkSpeed = 0.2f
    healthScale = 20.0
    resetMaxHealth()
    health = maxHealth
    resetPlayerTime()
    resetPlayerWeather()
    level = 0
    totalExperience = 0
    inventory.clear()
    inventory.armorContents = null
    canPickupItems = true
    fireTicks = 0
    activePotionEffects.forEach { removePotionEffect(it.type) }
}

fun Player.sendPacket(packet: PacketContainer) {
    try {
        ProtocolLibrary.getProtocolManager().sendServerPacket(this, packet)
    } catch (e: InvocationTargetException) {
        Bukkit.getLogger().log(Level.SEVERE, "Failed to send packet to ${this.uniqueId} (${this.name})", e)
    }
}

fun Player.isGamePlayer(): Boolean {
    return Game.players.firstOrNull { it.uuid == this.uniqueId } != null
}

fun Player.getGamePlayer(): GamePlayer? {
    return Game.players.firstOrNull { this.uniqueId == it.uuid }
}

fun OfflinePlayer.getOfflinePlayerDisplayName(): String {
    if (this.isOnline) return this.player.displayName
    return this.name ?: this.uniqueId.toString()
}

internal fun randInt(min: Int, max: Int) = ThreadLocalRandom.current().nextInt(min, max + 1)

internal fun chance(percent: Double) = Math.random() <= percent

fun World.dropItemFixed(location: Location, item: ItemStack) {
    val fixedLoc = Location(location.world, location.blockX + 0.5, location.blockY + 0.5, location.blockZ + 0.5)
    val itemEntity = this.dropItem(fixedLoc, item)
    itemEntity.velocity = Vector(0, 0, 0)
}

fun String.formatOwnership(): String = if (!this.endsWith("s")) this + "'s" else this + "'"