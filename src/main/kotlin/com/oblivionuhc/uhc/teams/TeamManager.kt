package com.oblivionuhc.uhc.teams

import com.oblivionuhc.uhc.exception.TeamException
import com.oblivionuhc.uhc.format
import com.oblivionuhc.uhc.formatOwnership
import com.oblivionuhc.uhc.game.Game
import com.oblivionuhc.uhc.game.GameState
import com.oblivionuhc.uhc.game.setting.team.TeamSizeGameSetting
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamType
import com.oblivionuhc.uhc.game.setting.team.teamType.TeamTypeGameSetting
import com.oblivionuhc.uhc.lang.LanguageManager
import com.oblivionuhc.uhc.lang.broadcast
import com.oblivionuhc.uhc.lang.send
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.scoreboard.Team
import java.util.*

internal object TeamManager {

    internal val teamNamePrefix = "UHC-"
    private val colors = LinkedList<ChatColor>()

    internal val teamColors = ChatColor.values().toList()
        .minus(listOf(ChatColor.ITALIC, ChatColor.BOLD, ChatColor.UNDERLINE, ChatColor.BLACK,
            ChatColor.DARK_BLUE, ChatColor.WHITE, ChatColor.RESET, ChatColor.MAGIC, ChatColor.STRIKETHROUGH))

    val scoreboard
        get() = Bukkit.getScoreboardManager().mainScoreboard

    val enabled: Boolean
        get() {
            val teamSizeSetting = Game.getSetting(TeamSizeGameSetting::class.java)
            val teamTypeSetting = Game.getSetting(TeamTypeGameSetting::class.java)

            return Game.state != GameState.RUNNING && if (teamSizeSetting != null) teamSizeSetting.value != 1 else false && if (teamTypeSetting != null) teamTypeSetting.value == TeamType.CHOSEN else false
        }

    private val invites = HashSet<TeamInvite>()

    fun Player.getTeamInvites() = invites.filter { it.invitedPlayer == this }

    /**
     * Send a team invite.
     * @param invitedPlayer The team to be invited to the invitingPlayer's team.
     * @throws TeamException
     */
    fun Player.sendTeamInvite(invitedPlayer: Player) {
        if (!enabled) {
            throw TeamException("Team management is not enabled.")
        }

        if (this == invitedPlayer) {
            throw TeamException("You cannot invite yourself to your own team.")
        }

        if (invitedPlayer.getTeam() != null) {
            throw TeamException("${invitedPlayer.name} is already on a team.")
        }

        val invitingPlayer = this

        val team = invitingPlayer.getTeam() ?: create(setOf(invitingPlayer))

        val maxTeamSize = Game.getSetting(TeamSizeGameSetting::class.java)?.value ?: 1

        if (team.size >= maxTeamSize) {
            throw TeamException("Your team is full.")
        }

        if (!invitedPlayer.getTeamInvites().none { it.invitingTeam == team }) {
            throw TeamException("${invitedPlayer.name} has already been invited to the team.")
        }

        invites.add(TeamInvite(invitedPlayer, invitingPlayer, team))

        LanguageManager.getMessage("team_invite_player", invitingPlayer)
            ?.replaceVariable("player", invitedPlayer.displayName)
            .send(invitingPlayer)

        LanguageManager.getMessage("team_invite", invitedPlayer)
            ?.replaceVariable("inviting_player", invitingPlayer.displayName.formatOwnership())
            .send(invitedPlayer)
    }

    /**
     * Accept a team invite.
     * @param invitingPlayer The player that sent the team invitation. Null is allowed if there is only 1 invite.
     * @throws TeamException
     */
    fun Player.acceptTeamInvite(invitingPlayer: Player? = null) {
        if (!enabled) {
            throw TeamException("Team management is not enabled.")
        }

        val invite: TeamInvite
        val invites = this.getTeamInvites()

        cleanInvalidInvitations()

        if (invitingPlayer == null) {
            when {
                invites.size == 1 -> invite = invites.first()
                invites.isEmpty() -> throw TeamException("You have no pending team invites.")
                else -> throw TeamException("You must specify a player because you have multiple pending team invites.")
            }
        } else {
            invite = invites.firstOrNull { it.invitingPlayer == invitingPlayer } ?: throw TeamException("You don't have any pending invites from that player.")
        }

        this@TeamManager.invites.remove(invite)
        invite.invitingTeam.addEntry(invite.invitedPlayer.name)

        invite.invitingTeam.entries.mapNotNull { Bukkit.getPlayer(it) }.forEach {
            LanguageManager.getMessage("player_joined_team", it)
                ?.replaceVariable("player", invite.invitedPlayer.displayName)
        }

        LanguageManager.getMessage("team_joined", invite.invitedPlayer)
            ?.replaceVariable("inviting_player", invite.invitingPlayer.displayName)
            .send(invite.invitedPlayer)
    }

    /**
     * Leave current team.
     * @throws TeamException
     */
    fun Player.leaveTeam() {
        if (!enabled) {
            throw TeamException("Team management is not enabled.")
        }

        val team = this.getTeam() ?: throw TeamException("You are not on a team.")
        team.removeEntry(this.name)

        LanguageManager.getMessage("leave_team", this).send(this)
    }

    /**
     * Reset team invites and teams.
     */
    fun reset() {
        clearInvites()

        for (team in scoreboard.teams) {
            if (team.name.startsWith(teamNamePrefix)) {
                team.unregister()
            }
        }

        broadcast("team_management_reset")
    }

    /**
     * Create a team.
     * @param players The players to add to rhe team.
     */
    fun create(players: Collection<Player>, name: String? = null, color: ChatColor? = null): Team {
        val team = create(name, color)
        players.forEach { team.addEntry(it.name) }

        return team
    }

    fun create(name: String? = null, color: ChatColor? = null): Team {
        if (name == null) {
            val randomName = UUID.randomUUID().toString().replace("-", "").slice(0 until 8)
            return create(randomName, color)
        }

        scoreboard.getTeam(teamNamePrefix + name) ?: return scoreboard.registerNewTeam(teamNamePrefix + name).apply { this.color = color ?: getNewTeamColor() }

        val team: Team

        var i = 2
        while (true) {
            val candidate = scoreboard.getTeam(teamNamePrefix + name + i)

            if (candidate == null) {
                team = scoreboard.registerNewTeam(teamNamePrefix + name + i)
                break
            }

            i++
        }

        team.color = color ?: getNewTeamColor()

        return team
    }

    /**
     * Remove all invitations from the list of pending initations.
     */
    fun clearInvites() {
        this.invites.clear()
    }

    /**
     * Remove invalid invitations from the list of pending invitations.
     */
    fun cleanInvalidInvitations() {
        if (!enabled) {
            invites.clear()
            return
        }

        val iter = invites.iterator()
        val maxTeamSize = Game.getSetting(TeamSizeGameSetting::class.java)?.value ?: 1

        while (iter.hasNext()) {
            val invite = iter.next()

            if (invite.isExpired) {
                iter.remove()
            } else if (!invite.invitingTeam.entries.contains(invite.invitingPlayer.name)) {
                iter.remove()
            } else if (invite.invitedPlayer.getTeam() != null) {
                iter.remove()
            } else if (invite.invitingTeam.size >= maxTeamSize) {
                iter.remove()
            }
        }
    }

    /**
     * Send a CommandSender the list of teams.
     */
    fun CommandSender.sendTeamList() {
        val teams = getTeams()
        if (teams.isEmpty()) {
            LanguageManager.getMessage("no_teams", this).send(this)
        } else {
            for (team in teams) {
                this.sendTeamInfo(team)
            }
        }
    }

    /**
     * Send the information of a team to a CommandSender
     * @param team The team whose information you want to send.
     * @throws TeamException
     */
    fun CommandSender.sendTeamInfo(team: Team) {
        if (!getTeams().contains(team)) {
            throw TeamException("That is not a UHC Team.")
        }

        LanguageManager.getMessage("team_info", this)
            ?.replaceVariable("team_number", team.getTeamNumer().toString())
            ?.replaceVariable("alive_players", team.entries.mapNotNull { entry -> Game.players.firstOrNull { entry == it.name } }.filter { it.alive }.format())
            ?.replaceVariable("dead_players", team.entries.mapNotNull { entry -> Game.players.firstOrNull { entry == it.name } }.filter { !it.alive }.format())
            .send(this)
    }

    /**
     * Send the information of a team to a CommandSender.
     * @param playerOnTeam The player that you want to get the team information of
     * @throws TeamException
     */
    fun CommandSender.sendTeamInfo(playerOnTeam: Player) {
        val team = playerOnTeam.getTeam() ?: throw TeamException("${playerOnTeam.name} is not on a UHC Team.")
        sendTeamInfo(team)
    }

    fun Player.getTeam(): Team? {
        val team = scoreboard.getEntryTeam(this.name) ?: return null

        if (!team.name.startsWith(teamNamePrefix)) {
            return null
        }

        return team
    }

    fun getTeams() = scoreboard.teams.filter { it.name.startsWith(teamNamePrefix) }.toSet()

    internal fun getNewTeamColor(): ChatColor {
        if (colors.isEmpty()) {
            repopulateTeamColors()
        }

        return colors.poll()
    }

    private fun repopulateTeamColors() {
        this.colors.addAll(teamColors)
    }

    fun Team.getTeamNumer(): Int {
        if (!getTeams().contains(this)) {
            throw TeamException("That is not a UHC Team.")
        }

        return Integer.parseInt(this.name.removePrefix(teamNamePrefix))
    }

    fun getTeam(teamNumber: Int) = scoreboard.teams.firstOrNull { it.getTeamNumer() == teamNumber }

    fun getTeam(name: String) = scoreboard.teams.firstOrNull { it.name == teamNamePrefix + name }

}