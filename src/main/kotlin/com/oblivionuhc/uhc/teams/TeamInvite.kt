package com.oblivionuhc.uhc.teams

import org.bukkit.entity.Player
import org.bukkit.scoreboard.Team
import java.time.LocalDateTime

internal data class TeamInvite(val invitedPlayer: Player, val invitingPlayer: Player, val invitingTeam: Team, val time: LocalDateTime = LocalDateTime.now()) {

    private val teamInviteExpirationSeconds: Long = 60

    val isExpired: Boolean
        get() = time.isBefore(LocalDateTime.now().minusSeconds(teamInviteExpirationSeconds))

}