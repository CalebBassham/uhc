package com.oblivionuhc.uhc.lang

import com.google.common.io.ByteSource
import com.oblivionuhc.uhc.UHC
import me.calebbassham.languages.loadLanguage
import me.calebbassham.languages.message.Message
import me.calebbassham.languages.message.MessageType
import me.calebbassham.languages.parser.JsonLanguageFileParser
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.io.InputStream
import java.util.logging.Level


object LanguageManager {
    private val messages = HashMap<String, Map<String, Message>>()
    private val parser = JsonLanguageFileParser()

    fun getMessage(name: String, lang: String?): Message? {
        val langMessages = messages[lang ?: "default"] ?: messages["default"]!!
        val msg = langMessages[name]

        if (msg == null) {
            UHC.logger.log(Level.SEVERE, "Message \"$name\" is not defined in language file $lang or default.")
        }

        return msg
    }

    fun getMessage(name: String, receiver: CommandSender) = getMessage(name, (receiver as? Player)?.locale)

    internal fun load(plugin: JavaPlugin) {
        // save defaults
        plugin.saveResource("lang/default/en.json", true)

        messages["default"] = loadLanguageFromResource("en", plugin)
        loadLanguagesFromPluginFolder(plugin)
    }

    private fun loadLanguageFromResource(name: String, plugin: JavaPlugin): Map<String, Message> =
        loadLanguage(plugin.getResource("lang/default/en.json").string(), parser,
            fun(message: Message) = Message(message.messageKey, ChatColor.translateAlternateColorCodes('&', message.message), message.messageType))

    private fun loadLanguageFromPluginFolder(name: String, plugin: JavaPlugin) {
        loadLanguage(File(plugin.dataFolder.path + File.separator + "lang" + File.separator + name).readText(), parser,
            fun(message: Message) = Message(message.messageKey, ChatColor.translateAlternateColorCodes('&', message.message), message.messageType))
    }

    private fun loadLanguagesFromPluginFolder(plugin: JavaPlugin) {
        File(plugin.dataFolder.path + File.separator + "lang").listFiles().filter { !it.isDirectory }.forEach {
            loadLanguage(it.readText(), parser, fun(message: Message) = Message(message.messageKey, ChatColor.translateAlternateColorCodes('&', message.message), message.messageType))
        }
    }

}

fun Message?.orUndefinedMessage(sender: CommandSender) = this
    ?: LanguageManager.getMessage("message_undefined", sender)!!

fun Message?.send(sender: CommandSender) {
    if (this == null) {
        this.orUndefinedMessage(sender).send(sender)
        return
    }

    when (this.messageType) {
        MessageType.PLAIN -> sender.sendMessage(this.message)
        MessageType.JSON -> {
            sender.spigot().sendMessage(*TextComponent.fromLegacyText(this.message))
        }
    }
}

//fun Message?.cmdException(sender: CommandSender, showSyntax: Boolean = false): InvalidCommandArgument {
//    if (this == null) {
//        return LanguageManager.getMessage("message_undefined", sender).cmdException(sender)
//    }
//
//    return InvalidCommandArgument(this.message, showSyntax)
//}

fun broadcast(messageKey: String, variables: Map<String, String>? = null) {
    Bukkit.getOnlinePlayers().forEach {
        var msg = LanguageManager.getMessage(messageKey, it)

        if (variables != null) {
            for ((key, value) in variables) {
                msg = msg?.replaceVariable(key, value)
            }
        }

        msg.send(it)
    }
}

fun InputStream.string() = object : ByteSource() {
    override fun openStream() = this@string
}.asCharSource(Charsets.UTF_8).read()